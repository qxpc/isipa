# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QFileDialog, QMessageBox
from ui_imitmodel import Ui_ImitModel
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QSize, Qt, QMimeData
from experiment_time import ExperimentTime

class ImitModel(QDialog):
    def __init__(self, lamb, m, counter, experiments, resObr, SPr):
        '''
        inc -- json с инцидентами
        lamb -- веса инцидентов
        m -- веса результатов обработки
        counter -- счетчик
        '''
        self.resObrab = resObr
        self.SPrime = SPr
        super(ImitModel, self).__init__()
        self.ui = Ui_ImitModel()
        self.ui.setupUi(self)
        self.setWindowTitle('Имитационная модель')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.setModal(True)
        self.ui.tabWidget.setCurrentIndex(0)
        self.ui.tabWidget.setTabText(0, "Результат обработки")
        self.ui.tabWidget.setTabText(1, "Время обработки")
        self.lamb = list(lamb)
        self.m = list(m)
        self.counter = list(counter)
        self.b = [1]
        self.experiments = experiments
        ##print(self.inc, self.lamb, self.m, self.counter, self.b)
        self.ui.okButton.clicked.connect(self.close)
        self.ui.saveButton.clicked.connect(self.saveToFile)
        self.ui.saveButton_2.clicked.connect(self.saveToFile_2)
        self.ui.stepbystepButton.clicked.connect(self.showTimeDialog)

        self.fillTable1()
        self.fillTable2()

    def fillTable2(self):
        table = self.ui.modelTable_2
        table.clear()
        self.columnNames_2 = ['Среднее время\nэскалации', "Среднее время\nсдерживания", 
        "Среднее время\nустранения", "Среднее время\nвосстановления",
        "Среднее время\nобработки"]
        table.setRowCount(len(self.counter))
        table.setColumnCount(len(self.columnNames_2))
        for i in range(len(self.columnNames_2)):
            table.horizontalHeader().setSectionResizeMode(i, 1)
        
        for i in enumerate(self.columnNames_2):
            table.setHorizontalHeaderItem(i[0], QTableWidgetItem(i[1]))
        for i in enumerate(self.counter):
            table.setVerticalHeaderItem(i[0], QTableWidgetItem(i[1][-1]))
            #self.experiments[incidentIndex][experimentValue][0]
            for k in range(len(self.columnNames_2)):
                sum = 0
                count = 0
                for e in range(len(self.experiments[0])):
                    if self.experiments[i[0]][e][k] != '-':
                        sum += self.experiments[i[0]][e][k]
                        count += 1
                if count != 0:
                    sum = round(sum / count, 2)
                else:
                    sum = '-'
                table.setItem(i[0], k, QTableWidgetItem(str(sum)))
                table.item(i[0], k).setFlags(Qt.ItemIsEnabled)
                    
                
        # for i in enumerate(self.inc):
            # self.ui.modelTable_2.setItem(i[0] + 1, 0, QTableWidgetItem(i[1]['inc']))
        
    def fillTable1(self):
        table = self.ui.modelTable
        table.clear()
        firstRow = ['Предотвращено\nЗМ', 'Успешно\nобработано',
                    'Обработано с\nнезначительными\nпоследствиями',
                    'Обработано со\nзначительными\nпоследствиями',
                    'Произошла\nэскалация', '', 'Вес инцидента']
        table.setRowCount(len(self.counter) + 3)
        table.setColumnCount(len(firstRow))
        
        for i in range(table.rowCount()):
            if i != table.columnCount() - 2:
                table.horizontalHeader().setSectionResizeMode(i, 1)
            for j in range(table.columnCount()):
                table.setItem(i, j, QTableWidgetItem(' '))
        table.horizontalHeader().resizeSection(table.columnCount() - 2, 32)
        
        for i in enumerate(firstRow):
            table.setHorizontalHeaderItem(i[0], QTableWidgetItem(i[1]))
            
        for i in enumerate(self.counter):
            table.setVerticalHeaderItem(i[0], QTableWidgetItem(i[1][-1]))
            for k in self.lamb: #заполнение весов инцидентов
                if i[1][-1] == k[0]:
                    table.setItem(i[0], table.columnCount() - 1, QTableWidgetItem(str(k[1])))
            
        table.setVerticalHeaderItem(len(self.counter) + 0, QTableWidgetItem(' '))
        table.setVerticalHeaderItem(len(self.counter) + 1, QTableWidgetItem('Интегральная оценка'))
        table.setVerticalHeaderItem(len(self.counter) + 2, QTableWidgetItem('Веса результатов обработки'))
        
        #заполнение счетчиков
        for i in enumerate(self.counter):
            for j in enumerate(i[1][:-1]):
                table.setItem(i[0], j[0], QTableWidgetItem(str(j[1])))
        #заполнение весов результатов обработки
        for i in enumerate(self.m):
            table.setItem(len(self.counter) + 2, i[0], QTableWidgetItem(str(i[1])))
        #заполнение b
        self.b = list()
        for k in range(5):
            self.b.append(0)
            for i in enumerate(self.counter):
                if i[1][k] != '-':
                    self.b[-1] += i[1][k] * float(table.item(i[0], table.columnCount() - 1).text())
                
        for i, j in enumerate(self.b):
            table.setItem(len(self.counter) + 1, i, QTableWidgetItem(str(j)))
            
        for i in range(table.rowCount()):
            for j in range(table.columnCount()):
                table.item(i, j).setFlags(Qt.ItemIsEnabled)
        
        # tmp = [list(i) for i in zip(*self.counter)]
        # for i in tmp:
            # self.b.append(sum([x*y for x, y in zip(i, self.lamb)]))
        # for i in enumerate(self.b):
            # self.ui.modelTable.setItem(len(self.inc) + 2, i[0] + 1, QTableWidgetItem(str(int(i[1] * 100) / 100)))
        #заполнение s
        self.S = self.calcS(self.b, self.m)

        self.SPrime = round(self.SPrime, 3)
        if self.S > self.SPrime:
            tmp = 'S = ' + str(round(self.S, 4)) + ' > ' + str(round(self.SPrime, 3))
            self.ui.imgLabel.setPixmap(QPixmap('./resources/tup.png').scaled(QSize(32, 32)))
        else:
            tmp = 'S = ' + str(round(self.S, 4)) + ' ≤ ' + str(round(self.SPrime, 3))
            self.ui.imgLabel.setPixmap(QPixmap('./resources/tdown.png').scaled(QSize(32, 32)))
        self.ui.estLabel.setText(tmp)

    def calcS(self, b, m):
        '''Подсчет s'''
        return b[0]*m[0] + b[1]*m[1] - b[2]*m[2] - b[3]*m[3] - b[4]*m[4]

    def saveToFile(self):
        '''сохранение таблицы в файл'''
        table = self.ui.modelTable
        tmp = ';'
        for i in range(table.columnCount()):
            tmp += table.horizontalHeaderItem(i).text().replace('\n', ' ') + ';'
        tmp += '\n'
        for i in range(table.rowCount()):
            tmp += table.verticalHeaderItem(i).text() + ';'
            for j in range(table.columnCount()):
               try:
                   tmp += table.item(i,j).text() + ";"
               except AttributeError:
                   tmp += ';'
            tmp += '\n'
        tmp += '\n' + 'S =' + ';' + str(round(self.S, 4))
        filename = QFileDialog.getSaveFileName(self, 'Сохранить в файл', './', '*.csv')
        if filename[0] != '':
            try:
                if filename[0][-4:] != '.csv':
                    with open(filename[0] + '.csv', 'w', encoding='utf8') as output:
                        output.write(tmp)
                else:
                    with open(filename[0], 'w', encoding='utf8') as output:
                        output.write(tmp)
            except WindowsError:  
                QMessageBox.critical(self, 'Ошибка', 'Файл используется другим процессом')
                self.saveToFile()

    def showTimeDialog(self):
        experTime = ExperimentTime()
        experTime.showDialog(self.counter, self.experiments, self.resObrab)

    def saveToFile_2(self):
        '''сохранение времени в файл'''
        tmp = ';'
        for i in self.columnNames_2:
            tmp += i.replace('\n', ' ') + ';'
        tmp += '\n'
        for i in range(self.ui.modelTable_2.rowCount()):
            tmp += self.counter[i][-1] + ';'
            for j in range(self.ui.modelTable_2.columnCount()):
                tmp += self.ui.modelTable_2.item(i,j).text() + ';'
            tmp = tmp[:len(tmp)-1] + '\n'
        filename = QFileDialog.getSaveFileName(self, 'Сохранить в файл', './', '*.csv')
        if filename[0] != '':
            try:
                if filename[0][-4:] != '.csv':
                    with open(filename[0] + '.csv', 'w', encoding='utf8') as output:
                        output.write(tmp)
                else:
                    with open(filename[0], 'w', encoding='utf8') as output:
                        output.write(tmp)
            except WindowsError:  
                QMessageBox.critical(self, 'Ошибка', 'Файл используется другим процессом')
                self.saveToFile()
