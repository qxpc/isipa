# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././newstandartdialog.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NewStandartDialog(object):
    def setupUi(self, NewStandartDialog):
        NewStandartDialog.setObjectName("NewStandartDialog")
        NewStandartDialog.resize(350, 120)
        NewStandartDialog.setMinimumSize(QtCore.QSize(350, 120))
        NewStandartDialog.setMaximumSize(QtCore.QSize(350, 120))
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(NewStandartDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(NewStandartDialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.name = QtWidgets.QLineEdit(NewStandartDialog)
        self.name.setObjectName("name")
        self.verticalLayout.addWidget(self.name)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(NewStandartDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(NewStandartDialog)
        self.buttonBox.accepted.connect(NewStandartDialog.accept)
        self.buttonBox.rejected.connect(NewStandartDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(NewStandartDialog)

    def retranslateUi(self, NewStandartDialog):
        _translate = QtCore.QCoreApplication.translate
        NewStandartDialog.setWindowTitle(_translate("NewStandartDialog", "Dialog"))
        self.label.setText(_translate("NewStandartDialog", "Название нового стандарта:"))

