# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QMessageBox, QTableWidgetItem, QFileDialog
from ui_outputdialog import Ui_OutputDialog
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon

class OutputDialog(QDialog):
    def __init__(self):
        super(OutputDialog, self).__init__()
        self.ui = Ui_OutputDialog()
        self.ui.setupUi(self)
        self.setModal(True)
        self.setWindowTitle("Вероятность видов инцидентов")
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.ui.tableInc.horizontalHeader().setDefaultSectionSize(150)
        self.ui.tableInc.horizontalHeader().setSectionResizeMode(0, 1)
        self.ui.tableInc.horizontalHeader().setSectionResizeMode(1, 2)
        self.ui.tableInc.horizontalHeader().setSectionResizeMode(2, 2)
        self.ui.tableInc.setSelectionMode(1)
        self.ui.tableInc.verticalHeader().resizeSection(0, 50)
        self.ui.saveButton.clicked.connect(self.saveToFile)
        
    def fill(self, inc, R_p, R_e, R_am, P_inc, P_inc_zm):
        self.ui.tableInc.setRowCount(len(inc))
        for i, j in enumerate(inc):
            #название
            self.ui.tableInc.setItem(i, 0, QTableWidgetItem(j['inc']))
            self.ui.tableInc.item(i, 0).setFlags(Qt.ItemIsEnabled)
            
            self.ui.tableInc.setItem(i, 1, QTableWidgetItem(str(float(P_inc[i]))))
            self.ui.tableInc.item(i, 1).setFlags(Qt.ItemIsEnabled)
            
            self.ui.tableInc.setItem(i, 2, QTableWidgetItem(str(float(P_inc_zm[i]))))
            self.ui.tableInc.item(i, 2).setFlags(Qt.ItemIsEnabled)
            
            self.ui.tableInc.setItem(i, 3, QTableWidgetItem(str(float(R_p[j['inc']]))))
            self.ui.tableInc.item(i, 3).setFlags(Qt.ItemIsEnabled)
            
            self.ui.tableInc.setItem(i, 4, QTableWidgetItem(str(float(R_e[j['inc']]))))
            self.ui.tableInc.item(i, 4).setFlags(Qt.ItemIsEnabled)
            
            self.ui.tableInc.setItem(i, 5, QTableWidgetItem(str(float(R_am[j['inc']]))))
            self.ui.tableInc.item(i, 5).setFlags(Qt.ItemIsEnabled)
            
    def saveToFile(self):
        '''сохранение таблицы в файл'''
        table = self.ui.tableInc
        tmp = ''
        for i in range(table.columnCount()):
            tmp += table.horizontalHeaderItem(i).text().replace('\n', ' ') + ';'
        tmp += '\n'
        for i in range(table.rowCount()):
            for j in range(table.columnCount()):
               try:
                   tmp += table.item(i,j).text() + ";"
               except AttributeError:
                   tmp += ';'
            tmp += '\n'
        
        filename = QFileDialog.getSaveFileName(self, 'Сохранить в файл', './', '*.csv')
        if filename[0] != '':
            try:
                if filename[0][-4:] != '.csv':
                    with open(filename[0] + '.csv', 'w', encoding='utf8') as output:
                        output.write(tmp)
                else:
                    with open(filename[0], 'w', encoding='utf8') as output:
                        output.write(tmp)
            except WindowsError:  
                QMessageBox.critical(self, 'Ошибка', 'Файл используется другим процессом')
                self.saveToFile()
                