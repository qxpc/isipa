# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_inc_1 import Ui_Edit_inc_1
from PyQt5.QtCore import Qt
from edit_inc_2 import Edit_inc_2
from edit_matrix import Edit_matrix
from edit_time import Edit_time
import copy

class Edit_inc_1(QDialog):
    def __init__(self):
        super(Edit_inc_1, self).__init__()
        self.ui = Ui_Edit_inc_1()
        self.ui.setupUi(self)
        self.inc = []
        self.out = None
        self.setWindowTitle('Редактирование: Вид инцидента')
        self.setWindowIcon(QIcon('resources/icon.png'))
        
        self.ui.ns2Table.setColumnCount(2)
        self.ui.ns2Table.setSelectionMode(1)
        self.ui.ns2Table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.ns2Table.verticalHeader().setVisible(False)
        self.ui.ns2Table.horizontalHeader().setVisible(False)
        
        #события
        self.ui.addNs2Button.clicked.connect(self.addNs2)
        self.ui.editNs2Button.clicked.connect(self.editNs2)
        self.ui.matrixButton.clicked.connect(self.editMatrix)
        self.ui.openTminButton.clicked.connect(self.editTime)
        self.ui.remNs2Button.clicked.connect(self.remNs2)
        
        self.ui.ns2Table.itemChanged.connect(self.saveChangedTableItems)
        
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        
    def update(self, inc):
        self.inc = copy.deepcopy(inc)
        self.ui.selectedInc.setText(inc['inc']) #выбранный вид инцидента
        self.fillNs2Table()
    
    def buttonSetEnabled(self, status):
        self.ui.remNs2Button.setEnabled(status)
        self.ui.editNs2Button.setEnabled(status)
        
    def fillNs2Table(self):
        """Заполнение"""
        '''Нежелательные события второго уровня'''
        self.ui.ns2Table.clearContents()
        self.ui.ns2Table.setRowCount(0)
        
        if len(self.inc['ns2']) == 0:
            self.buttonSetEnabled(False)
        else:
            self.buttonSetEnabled(True)
        
        for i in self.inc['ns2']:
            self.ui.ns2Table.insertRow(self.ui.ns2Table.rowCount())
            self.ui.ns2Table.setItem(self.ui.ns2Table.rowCount() - 1, 0, QTableWidgetItem(i['name']))
            self.ui.ns2Table.item(self.ui.ns2Table.rowCount() - 1, 0).setToolTip(i['name'])
            self.ui.ns2Table.setItem(self.ui.ns2Table.rowCount() - 1, 1, QTableWidgetItem(str(i['ver'])))
            
    def editNs2(self):
        '''редактирование выбранного НС2'''
        winEdit2 = Edit_inc_2()
        row = self.ui.ns2Table.currentRow()
        winEdit2.update(self.inc['ns2'][row])
        winEdit2.move(self.x() + 25, self.y() + 25)
        winEdit2.exec_()
        
        if winEdit2.out is not None:
            self.inc['ns2'][row] = copy.deepcopy(winEdit2.out)
            self.fillNs2Table()
    
    def editMatrix(self):
        winMatrix = Edit_matrix()
        winMatrix.update(self.inc['matrix'])
        winMatrix.exec_()

        if winMatrix.out is not None:
            self.inc['matrix'] = copy.deepcopy(winMatrix.out)
            
    def editTime(self):
        winTime = Edit_time()
        winTime.update(self.inc['tmin'])
        winTime.exec_()

        if winTime.out is not None:
            self.inc['tmin'] = copy.deepcopy(winTime.out)
    
    def addNs2(self):   
        winEdit2 = Edit_inc_2()
        winEdit2.update({'ns1': [], 'fac': [], 'name': '', 'ver': ''})
        winEdit2.move(self.x() + 25, self.y() + 25)
        winEdit2.exec_()
    
        if winEdit2.out is not None:
            self.inc['ns2'].append(copy.deepcopy(winEdit2.out))
            self.fillNs2Table()
    
    def remNs2(self):
        if self.ui.ns2Table.currentRow() >= 0:
            self.ui.ns2Table.removeRow(self.ui.ns2Table.currentRow())
            del self.inc['ns2'][self.ui.ns2Table.currentRow()]
            if len(self.inc['ns2']) == 0:
                self.buttonSetEnabled(False)
    
    def saveChangedTableItems(self, item):
        '''сохранить изменения из списка в inc'''
        if item.text() != "":
            if item.column() == 1:
                try:
                    tmp = float(item.text().replace(',', '.'))
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                    item.setText(str(self.inc['ns2'][item.row()]['ver']))
                else:
                    if (tmp > 1) or (tmp < 0):
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                        item.setText(str(self.inc['ns2'][item.row()]['ver']))
                    else:
                        self.inc['ns2'][item.row()]['ver'] = tmp
            elif item.column() == 0:
                self.inc['ns2'][item.row()]['name'] = item.text()
                item.setToolTip(item.text())
        else:
            if item.column() == 1:
                item.setText(str(self.inc['ns2'][item.row()]['ver']))
            else:
                item.setText(self.inc['ns2'][item.row()]['name'])

    def save(self):
        tmp = self.ui.selectedInc.text()
        if tmp == '':
            QMessageBox.critical(self, 'Ошибка', 'Укажите название вида инцидента')
            self.ui.selectedInc.setFocus(3)
            return 1
        if self.ui.ns2Table.rowCount() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список нежелательных событий пуст')
            return 1

        if self.inc['tmin'] == [[],[],[]]:
            QMessageBox.critical(self, 'Ошибка', 'Задайте перечень процедур')
            return 1
            
        self.inc['inc'] = tmp
        self.out = self.inc
        self.close()
