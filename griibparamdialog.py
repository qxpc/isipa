# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QMessageBox
from ui_griibparamdialog import Ui_GriibParamDialog
from PyQt5.QtGui import QIcon

class GriibParamDialog(QDialog):
    def __init__(self):
        super(GriibParamDialog, self).__init__()
        self.ui = Ui_GriibParamDialog()
        self.ui.setupUi(self)
        self.setWindowTitle('Выбор параметра ГРИИБ')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.ui.okButton.clicked.connect(self.close)
        self.ui.radioButton.clicked.connect(self.showText)
        self.ui.radioButton_2.clicked.connect(self.showText)
        self.ui.radioButton_3.clicked.connect(self.showText)
        self.text1 = ''
        self.text2 = ''
        self.text3 = ''
        self.loadText()

    def getCoef(self):
        if self.ui.radioButton.isChecked():
            return 2
        elif self.ui.radioButton_2.isChecked():
            return 1.5
        elif self.ui.radioButton_3.isChecked():
            return 1
    
    def setCoef(self, coef):
        if coef == 2:
            self.ui.radioButton.setChecked(True)
        elif coef == 1.5:
            self.ui.radioButton_2.setChecked(True)
        elif coef == 1:
            self.ui.radioButton_3.setChecked(True)
        self.showText()

    def showText(self):
        if self.ui.radioButton.isChecked():
            self.ui.textBrowser.clear()
            self.ui.textBrowser.setText(self.text1)
        elif self.ui.radioButton_2.isChecked():
            self.ui.textBrowser.clear()
            self.ui.textBrowser.setText(self.text2)
        elif self.ui.radioButton_3.isChecked():
            self.ui.textBrowser.clear()
            self.ui.textBrowser.setText(self.text3)

    def loadText(self):
        try:
            with open('./resources/griib/text1', 'r', encoding='utf8') as file1:
                self.text1 = file1.read()
            with open('./resources/griib/text2', 'r', encoding='utf8') as file2:
                self.text2 = file2.read()
            with open('./resources/griib/text3', 'r', encoding='utf8') as file3:
                self.text3 = file3.read()
        except Exception:
            QMessageBox.warning(self, "Ошибка!", "Отсутствуют файлы с описанием параметров ГРИИБ")
    
