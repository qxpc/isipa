# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_inc_0 import Ui_Edit_inc_0
from PyQt5.QtCore import Qt
from edit_inc_1 import Edit_inc_1
from ask_weight import Ask_weight
import json, copy

class Edit_inc_0(QDialog):
    def __init__(self):
        super(Edit_inc_0, self).__init__()
        self.ui = Ui_Edit_inc_0()
        self.ui.setupUi(self)
        self.inc = []
        self.outInc = None
        self.outReq = None
        self.setWindowTitle('Редактирование: Перечень видов инцидентов')
        self.setWindowIcon(QIcon('resources/icon.png'))
        
        self.ui.addIncButton.clicked.connect(self.addInc)
        self.ui.editIncButton.clicked.connect(self.editInc)
        self.ui.editWghtButton.clicked.connect(self.editWght)
        self.ui.remIncButton.clicked.connect(self.remInc)
        
        self.ui.incList.itemChanged.connect(self.saveChangedListItem)
        
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        
    def update(self, inc, req):
        self.inc = copy.deepcopy(inc)
        self.req = copy.deepcopy(req)
        self.fillIncidentList()
    
    def buttonSetEnabled(self, status):
        self.ui.remIncButton.setEnabled(status)
        self.ui.editIncButton.setEnabled(status)
        self.ui.editWghtButton.setEnabled(status)
    
    def fillIncidentList(self):
        '''Инциденты'''
        self.ui.incList.clear()
        if len(self.inc) == 0:
            self.buttonSetEnabled(False)
        else:
            self.buttonSetEnabled(True)
        for i in self.inc:
            self.ui.incList.addItem(i['inc'])
            self.ui.incList.setCurrentRow(self.ui.incList.count() - 1)
            self.ui.incList.currentItem().setToolTip(i['inc'])
            self.ui.incList.currentItem().setFlags(
                self.ui.incList.currentItem().flags() | Qt.ItemIsEditable)
        self.ui.incList.setCurrentRow(0)
                
    def editInc(self):
        '''редактирование выбранного инцидента'''
        winEdit1 = Edit_inc_1()
        row = self.ui.incList.currentRow()
        winEdit1.update(self.inc[row])
        winEdit1.move(self.x() + 25, self.y() + 25)
        winEdit1.exec_()

        if winEdit1.out is not None:
            self.inc[row] = copy.deepcopy(winEdit1.out)
            self.fillIncidentList()
            
    def editWght(self):
        winWeight = Ask_weight()
        row = self.ui.incList.currentRow()
        winWeight.update(self.req, self.inc[row]['inc'])
        winWeight.move(self.x() + 25, self.y() + 25)
        winWeight.exec_()
        
        if winWeight.out is not None:
            self.req = copy.deepcopy(winWeight.out)
                
    def addInc(self):
        '''Добавление нового инцидента'''
        winEdit1 = Edit_inc_1()
        winEdit1.update({'inc': '', 'ns2': [], 'tmin': [[],[],[]], 'matrix': [[1,2,3],[2,3,4],[3,4,5],[4,5,6],[5,6,7]]})
        winEdit1.move(self.x() + 25, self.y() + 25)
        winEdit1.exec_()

        if winEdit1.out is not None:
            self.inc.append(copy.deepcopy(winEdit1.out))
            self.fillIncidentList()
            self.ui.incList.setCurrentRow(self.ui.incList.count() - 1)
            self.editWght()
        
    def remInc(self):
        '''Удаляет инцидент'''
        if self.ui.incList.currentRow() >= 0:
            for i in range(len(self.req)):
                for j in range(len(self.req[i]["reqList"])):
                    for k in range(len(self.req[i]["reqList"][j]['inc'])):
                        if self.req[i]["reqList"][j]['inc'][k]['name'] == self.inc[self.ui.incList.currentRow()]['inc']:
                            del self.req[i]["reqList"][j]['inc'][k]
                            break
        
            del self.inc[self.ui.incList.currentRow()]
            self.ui.incList.takeItem(self.ui.incList.currentRow())
        if len(self.inc) == 0:
            self.buttonSetEnabled(False)
            
    def saveChangedListItem(self, item):
        '''сохранить изменения из списка в inc'''
        if item.text() != "":
            self.inc[item.listWidget().row(item)]['inc'] = item.text()
            item.setToolTip(item.text())
        else:
            item.setText(self.inc[item.listWidget().row(item)]['inc'])
            

    def save(self):
        '''закрыть с сохранением'''
        if self.ui.incList.count() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список видов инцидентов пуст')
            return 1
        self.outInc = self.inc
        self.outReq = self.req
        self.close()
