# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././outputdialog.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OutputDialog(object):
    def setupUi(self, OutputDialog):
        OutputDialog.setObjectName("OutputDialog")
        OutputDialog.resize(964, 304)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(OutputDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableInc = QtWidgets.QTableWidget(OutputDialog)
        self.tableInc.setRowCount(4)
        self.tableInc.setColumnCount(6)
        self.tableInc.setObjectName("tableInc")
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableInc.setHorizontalHeaderItem(5, item)
        self.tableInc.horizontalHeader().setVisible(True)
        self.verticalLayout.addWidget(self.tableInc)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.saveButton = QtWidgets.QPushButton(OutputDialog)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(OutputDialog)
        QtCore.QMetaObject.connectSlotsByName(OutputDialog)

    def retranslateUi(self, OutputDialog):
        _translate = QtCore.QCoreApplication.translate
        OutputDialog.setWindowTitle(_translate("OutputDialog", "Dialog"))
        item = self.tableInc.horizontalHeaderItem(0)
        item.setText(_translate("OutputDialog", "Вид инцидента"))
        item = self.tableInc.horizontalHeaderItem(1)
        item.setText(_translate("OutputDialog", "Вероятность"))
        item = self.tableInc.horizontalHeaderItem(2)
        item.setText(_translate("OutputDialog", "Вероятность\n"
"с учетом ЗМ"))
        item = self.tableInc.horizontalHeaderItem(3)
        item.setText(_translate("OutputDialog", "Результативность\n"
"превентивных ЗМ"))
        item = self.tableInc.horizontalHeaderItem(4)
        item.setText(_translate("OutputDialog", "Результативность\n"
"процедур реагирования"))
        item = self.tableInc.horizontalHeaderItem(5)
        item.setText(_translate("OutputDialog", "Результативность\n"
"антикризисных мер"))
        self.saveButton.setText(_translate("OutputDialog", "Сохранить в файл..."))

