#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
#sys.path.append("./ui")
from PyQt5.QtWidgets import QApplication
from mainwindow import MainWindow


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()

    window.show()
    sys.exit(app.exec_())
