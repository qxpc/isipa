# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QHeaderView, QMessageBox
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QIcon
from ui_incidentprobabilities import Ui_IncidentProbabilities
from decimal import Decimal
import copy

class IncidentProbabilities(QDialog):
    def __init__(self, incWeight, procRes, SPr):
        '''
        inc -- json с инцидентами;
        incWeight -- вес инцидентов;
        procRes -- результат обработки
        '''
        super(IncidentProbabilities, self).__init__()
        self.ui = Ui_IncidentProbabilities()
        self.ui.setupUi(self)
        self.setWindowTitle('Шкала')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.ui.incProb.setColumnCount(2)
        self.ui.incProb.setHorizontalHeaderItem(0, QTableWidgetItem('Название'))
        self.ui.incProb.setHorizontalHeaderItem(1, QTableWidgetItem('Вес'))
        self.ui.procResult.setColumnCount(2)
        self.ui.procResult.setRowCount(6)
        self.ui.procResult.setHorizontalHeaderItem(0, QTableWidgetItem('Результат'))
        self.ui.procResult.setHorizontalHeaderItem(1, QTableWidgetItem('Вес'))
        self.incRes = copy.deepcopy(incWeight)
        self.resRes = copy.deepcopy(procRes)
        self.SPrime = SPr
        self.ui.spinBoxSPrime.setValue(SPr)
        self.out1, self.out2 = [], []
        self.ui.incProb.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.procResult.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.okButton.clicked.connect(self.finish)
        self.ui.cancelButton.clicked.connect(self.cancel)
        self.ui.incProb.itemChanged.connect(self.calcProbItem)
        self.ui.procResult.itemChanged.connect(self.calcProbItem)
        self.fillTable()

    def fillTable(self):
        self.ui.incProb.setRowCount(len(self.incRes)+1)
        #Заполнение видов инцидентов
        for i in enumerate(self.incRes):
            self.ui.incProb.setItem(i[0], 0, QTableWidgetItem(i[1][0]))
            self.ui.incProb.item(i[0], 0).setFlags(
                self.ui.incProb.item(i[0],0).flags() & ~Qt.ItemIsEditable)
        self.ui.incProb.setItem(self.ui.incProb.rowCount() - 1, 0, QTableWidgetItem('Сумма'))
        self.ui.incProb.item(self.ui.incProb.rowCount() - 1, 0).setFlags(
            self.ui.incProb.item(self.ui.incProb.rowCount() - 1,0).flags() & ~Qt.ItemIsEditable)
        self.ui.incProb.setItem(self.ui.incProb.rowCount() - 1, 1, QTableWidgetItem())
        self.ui.incProb.item(self.ui.incProb.rowCount() - 1, 1).setFlags(
            self.ui.incProb.item(self.ui.incProb.rowCount() - 1,0).flags() & ~Qt.ItemIsEditable)
        #заполнение результатов обработки и их весов
        resultColumn = ['Предотвращено ЗМ', 'Успешно обработано',
                        'Обрабка с незначительными последствиями',
                        'Обрабка со значительными последствиями',
                        'Произошла эскалация', 'Сумма']
        resultWeight = [0.3, 0.15, 0.1, 0.15, 0.3]
        for i in range(6):
            self.ui.procResult.setItem(i, 0, QTableWidgetItem(resultColumn[i]))
            self.ui.procResult.item(i, 0).setFlags(
                self.ui.procResult.item(i, 0).flags() & ~Qt.ItemIsEditable)
        if self.resRes == []:
            for i in enumerate(resultWeight):
                self.ui.procResult.setItem(i[0], 1, QTableWidgetItem(str(i[1])))
            self.ui.procResult.setItem(self.ui.procResult.rowCount() - 1, 1,
                                       QTableWidgetItem(str(sum(resultWeight))))
            self.resRes = list(resultWeight)
        else:
            for i in enumerate(self.resRes):
                self.ui.procResult.setItem(i[0], 1, QTableWidgetItem(str(i[1])))
            self.ui.procResult.setItem(self.ui.procResult.rowCount() - 1, 1,
                                       QTableWidgetItem(str(sum(resultWeight))))
        
        for i in enumerate(self.incRes):
            self.ui.incProb.setItem(i[0], 1, QTableWidgetItem(str(i[1][1])))

    def finish(self):
        self.incRes.clear()
        self.resRes.clear()
        try:
            for i in range(self.ui.incProb.rowCount() - 1):
                self.incRes.append([self.ui.incProb.item(i, 0).text(), float(self.ui.incProb.item(i, 1).text().replace(",", "."))])
            for i in range(self.ui.procResult.rowCount() - 1):
                self.resRes.append(float(self.ui.procResult.item(i, 1).text().replace(",", ".")))
            self.incRes = [[i[0], float(i[1])] for i in self.incRes]
            self.resRes = [float(i) for i in self.resRes]
        except ValueError:
            QMessageBox.critical(self, 'Ошибка', 'В таблице можно указывать только числа в интервале от 0 до 1')
        if sum([Decimal(str(i[1])) for i in self.incRes]) != 1.0 or sum([Decimal(str(i)) for i in self.resRes]) != 1.0:
            QMessageBox.critical(self, 'Ошибка', 'Сумма весов не равна 1')
        else:
            self.out1 = self.incRes
            self.out2 = self.resRes
            self.SPrime = self.ui.spinBoxSPrime.value()
            self.close()

    def cancel(self):
        self.incRes, self.resRes = [], []
        self.close()
    
    def calcProbItem(self, item):
        '''подсчет общей вероятности'''
        if item.tableWidget() is self.ui.incProb:
            if item.column() == 1 and item.row() != self.ui.incProb.rowCount() - 1:
                tmp = 0
                try:
                    tmp1 = float(item.text())
                    if tmp1 > 1 or tmp1 < 0:
                        QMessageBox.critical(self, 'Ошибка', 'В таблице можно указывать только числа в интервале от 0 до 1')
                        item.setText(str(self.incRes[item.row()][1]))
                    else:
                        self.incRes[item.row()][1] = tmp1
                        for i in range(self.ui.incProb.rowCount() - 1):
                            tmp += float(self.ui.incProb.item(i, 1).text().replace(",", ".")) * 100
                        self.ui.incProb.item(self.ui.incProb.rowCount() - 1, 1).setText(str(int(tmp) / 100))
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', 'В таблице можно указывать только числа в интервале от 0 до 1')
                    item.setText(str(self.incRes[item.row()][1]))
                except NameError:
                    None
                except AttributeError:
                    None
        elif item.tableWidget() is self.ui.procResult:
            if item.column() == 1 and item.row() != self.ui.procResult.rowCount() - 1:
                tmp = 0
                try:
                    tmp1 = float(item.text())
                    if tmp1 > 1 or tmp1 < 0:
                        QMessageBox.critical(self, 'Ошибка', 'В таблице можно указывать только числа в интервале от 0 до 1')
                        item.setText(str(self.resRes[item.row()]))
                    else:
                        self.resRes[item.row()] = tmp1
                        for i in range(self.ui.procResult.rowCount() - 1):
                            tmp += float(self.ui.procResult.item(i, 1).text().replace(",", ".")) * 100
                        self.ui.procResult.item(self.ui.procResult.rowCount() - 1, 1).setText(str(int(tmp) / 100))
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', 'В таблице можно указывать только числа в интервале от 0 до 1')
                    item.setText(str(self.resRes[item.row()]))
                except NameError:
                    None
                except AttributeError:
                    None
            
