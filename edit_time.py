# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_time import Ui_Edit_time
from PyQt5.QtCore import Qt
import copy

class Edit_time(QDialog):
    def __init__(self):
        super(Edit_time, self).__init__()
        self.ui = Ui_Edit_time()
        self.ui.setupUi(self)
        self.setWindowTitle('Редактирование: Перечень процедур реагирования')
        self.setWindowIcon(QIcon('resources/icon.png'))

        self.ui.timeTable1.setSelectionMode(1)
        self.ui.timeTable1.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.timeTable2.setSelectionMode(1)
        self.ui.timeTable2.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.timeTable3.setSelectionMode(1)
        self.ui.timeTable3.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)

        self.ui.timeTable1.itemChanged.connect(self.saveChangedTableItems)
        self.ui.timeTable2.itemChanged.connect(self.saveChangedTableItems)
        self.ui.timeTable3.itemChanged.connect(self.saveChangedTableItems)
        self.ui.addButton1.clicked.connect(self.addRow1)
        self.ui.delButton1.clicked.connect(self.delRow1)
        self.ui.addButton2.clicked.connect(self.addRow2)
        self.ui.delButton2.clicked.connect(self.delRow2)
        self.ui.addButton3.clicked.connect(self.addRow3)
        self.ui.delButton3.clicked.connect(self.delRow3)
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        self.out = None
        
    def update(self, m):
        self.time = copy.deepcopy(m)
        self.fillMatrix()
        
    def fillMatrix(self):
        self.ui.timeTable1.clearContents()
        self.ui.timeTable1.setRowCount(0)        
        for i in self.time[0]:
            self.ui.timeTable1.insertRow(self.ui.timeTable1.rowCount())
            self.ui.timeTable1.setItem(self.ui.timeTable1.rowCount() - 1, 0, QTableWidgetItem(i['name']))
            self.ui.timeTable1.item(self.ui.timeTable1.rowCount() - 1, 0).setToolTip(i['name'])
            self.ui.timeTable1.setItem(self.ui.timeTable1.rowCount() - 1, 1, QTableWidgetItem(str(i['time'])))
            
        self.ui.timeTable2.clearContents()
        self.ui.timeTable2.setRowCount(0)        
        for i in self.time[1]:
            self.ui.timeTable2.insertRow(self.ui.timeTable2.rowCount())
            self.ui.timeTable2.setItem(self.ui.timeTable2.rowCount() - 1, 0, QTableWidgetItem(i['name']))
            self.ui.timeTable2.item(self.ui.timeTable2.rowCount() - 1, 0).setToolTip(i['name'])
            self.ui.timeTable2.setItem(self.ui.timeTable2.rowCount() - 1, 1, QTableWidgetItem(str(i['time'])))
            
        self.ui.timeTable3.clearContents()
        self.ui.timeTable3.setRowCount(0)        
        for i in self.time[2]:
            self.ui.timeTable3.insertRow(self.ui.timeTable3.rowCount())
            self.ui.timeTable3.setItem(self.ui.timeTable3.rowCount() - 1, 0, QTableWidgetItem(i['name']))
            self.ui.timeTable3.item(self.ui.timeTable3.rowCount() - 1, 0).setToolTip(i['name'])
            self.ui.timeTable3.setItem(self.ui.timeTable3.rowCount() - 1, 1, QTableWidgetItem(str(i['time'])))

    def addRow1(self):
        self.addRow(self.ui.timeTable1, 0)
        
    def delRow1(self):
        self.delRow(self.ui.timeTable1, 0)
        
    def addRow2(self):
        self.addRow(self.ui.timeTable2, 1)
        
    def delRow2(self):
        self.delRow(self.ui.timeTable2, 1)
    
    def addRow3(self):
        self.addRow(self.ui.timeTable3, 2)
        
    def delRow3(self):
        self.delRow(self.ui.timeTable3, 2)

    def addRow(self, table, i):
        self.time[i].append({'name': '<процедура>', 'time': 1})
        table.insertRow(table.rowCount())
        table.setItem(table.rowCount() - 1, 0, QTableWidgetItem('<процедура>'))
        table.item(table.rowCount() - 1, 0).setToolTip('<процедура>')
        table.setItem(table.rowCount() - 1, 1, QTableWidgetItem('1'))
            
        table.setCurrentCell(table.rowCount() - 1, 0)
        
    def delRow(self, table, i):
        if table.rowCount() > 1:
            table.removeRow(table.currentRow())
            del self.time[i][table.currentRow()]
            
    def saveChangedTableItems(self, item):
        '''сохранить изменения из списка в inc'''
        if item.tableWidget() is self.ui.timeTable1:
            i = 0
        elif item.tableWidget() is self.ui.timeTable2:
            i = 1
        elif item.tableWidget() is self.ui.timeTable3:
            i = 2
        
        if item.text() != '':
            if item.column() == 0:
                self.time[i][item.row()]['name'] = item.text()
            elif item.column() == 1:
                try:
                    tmp = int(item.text())
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть натуральное число")
                    item.setText(str(self.time[i][item.row()]['time']))
                else:
                    if tmp < 1:
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть натуральное число")
                        item.setText(str(self.time[i][item.row()]['time']))
                    else:
                        self.time[i][item.row()]['time'] = tmp
        else:
            if item.column() == 0:
                item.setText(self.time[i][item.row()]['name'])
            elif item.column() == 1:
                item.setText(str(self.time[i][item.row()]['time']))
        
    def save(self):
        if self.ui.timeTable1.rowCount() == 0 or self.ui.timeTable2.rowCount() == 0 or self.ui.timeTable3.rowCount() == 0:
            QMessageBox.critical(self, 'Ошибка', "Перечни процедур пусты")
        else:
            self.out = self.time
            self.close()
