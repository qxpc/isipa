# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././imitmodel.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ImitModel(object):
    def setupUi(self, ImitModel):
        ImitModel.setObjectName("ImitModel")
        ImitModel.resize(935, 416)
        self.verticalLayout = QtWidgets.QVBoxLayout(ImitModel)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabWidget = QtWidgets.QTabWidget(ImitModel)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.modelTable = QtWidgets.QTableWidget(self.tab)
        self.modelTable.setObjectName("modelTable")
        self.modelTable.setColumnCount(0)
        self.modelTable.setRowCount(0)
        self.verticalLayout_2.addWidget(self.modelTable)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.estLabel = QtWidgets.QLabel(self.tab)
        self.estLabel.setObjectName("estLabel")
        self.horizontalLayout.addWidget(self.estLabel)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.imgLabel = QtWidgets.QLabel(self.tab)
        self.imgLabel.setObjectName("imgLabel")
        self.horizontalLayout.addWidget(self.imgLabel)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.saveButton = QtWidgets.QPushButton(self.tab)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.modelTable_2 = QtWidgets.QTableWidget(self.tab_2)
        self.modelTable_2.setObjectName("modelTable_2")
        self.modelTable_2.setColumnCount(0)
        self.modelTable_2.setRowCount(0)
        self.verticalLayout_3.addWidget(self.modelTable_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.stepbystepButton = QtWidgets.QPushButton(self.tab_2)
        self.stepbystepButton.setObjectName("stepbystepButton")
        self.horizontalLayout_3.addWidget(self.stepbystepButton)
        self.saveButton_2 = QtWidgets.QPushButton(self.tab_2)
        self.saveButton_2.setObjectName("saveButton_2")
        self.horizontalLayout_3.addWidget(self.saveButton_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.tabWidget.addTab(self.tab_2, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem4)
        self.okButton = QtWidgets.QPushButton(ImitModel)
        self.okButton.setObjectName("okButton")
        self.horizontalLayout_2.addWidget(self.okButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ImitModel)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(ImitModel)

    def retranslateUi(self, ImitModel):
        _translate = QtCore.QCoreApplication.translate
        ImitModel.setWindowTitle(_translate("ImitModel", "Dialog"))
        self.estLabel.setText(_translate("ImitModel", "TextLabel"))
        self.imgLabel.setText(_translate("ImitModel", "TextLabel"))
        self.saveButton.setText(_translate("ImitModel", "Сохранить в файл..."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("ImitModel", "Tab 1"))
        self.stepbystepButton.setText(_translate("ImitModel", "Пошагово..."))
        self.saveButton_2.setText(_translate("ImitModel", "Сохранить в файл..."))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("ImitModel", "Tab 2"))
        self.okButton.setText(_translate("ImitModel", "ОК"))

