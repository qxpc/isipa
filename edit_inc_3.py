# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_inc_3 import Ui_Edit_inc_3
from PyQt5.QtCore import Qt
import copy

class Edit_inc_3(QDialog):
    def __init__(self):
        super(Edit_inc_3, self).__init__()
        self.ui = Ui_Edit_inc_3()
        self.ui.setupUi(self)
        self.ns1 = []
        self.out = None
        self.setWindowTitle('Редактирование: Нежелательное событие 1-го уровня')
        self.setWindowIcon(QIcon('resources/icon.png'))
        
        self.ui.selectedNs1.setColumnCount(2)
        self.ui.selectedNs1.setSelectionMode(1)
        self.ui.selectedNs1.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.selectedNs1.verticalHeader().setVisible(False)
        self.ui.selectedNs1.horizontalHeader().setVisible(False)
        
        #события
        self.ui.remFactor1Button.clicked.connect(self.remFac1)
        self.ui.addFactor1Button.clicked.connect(self.addFac1)
        
        self.ui.factor1List.itemChanged.connect(self.saveChangedListItem)
        self.ui.selectedNs1.itemChanged.connect(self.saveChangedTableItems)
        
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        
    def update(self, ns1):
        self.ns1 = copy.deepcopy(ns1)
        self.ui.selectedNs1.setItem(0, 0, QTableWidgetItem(ns1['name']))    # выбранное
        self.ui.selectedNs1.item(0, 0).setToolTip(ns1['name'])
        self.ui.selectedNs1.setItem(0, 1, QTableWidgetItem(str(ns1['ver']))) #          НС2
        
        self.fillFactor1List()
        
    def fillFactor1List(self):
        """Заполнение списка факторов"""
        self.ui.factor1List.clear()
        if len(self.ns1['fac']) == 0:
            self.ui.remFactor1Button.setEnabled(False)
        else:
            self.ui.remFactor1Button.setEnabled(True)
        for i in self.ns1['fac']:
            self.ui.factor1List.addItem(i)
            self.ui.factor1List.setCurrentRow(self.ui.factor1List.count() - 1)
            self.ui.factor1List.currentItem().setToolTip(i)
            self.ui.factor1List.currentItem().setFlags(
                self.ui.factor1List.currentItem().flags() | Qt.ItemIsEditable)

    def addFac1(self):
        self.ui.remFactor1Button.setEnabled(True)
        self.ns1['fac'].append('<Фактор>')
        self.ui.factor1List.addItem('<Фактор>')
        self.ui.factor1List.setCurrentRow(self.ui.factor1List.count() - 1)
        self.ui.factor1List.currentItem().setFlags(
            self.ui.factor1List.currentItem().flags() | Qt.ItemIsEditable)
        
        self.ui.factor1List.editItem(self.ui.factor1List.currentItem())
                
    def remFac1(self):
        '''Удаляет инцидент'''
        if self.ui.factor1List.count() != 0:
            del self.ns1['fac'][self.ui.factor1List.currentRow()]
            self.ui.factor1List.takeItem(self.ui.factor1List.currentRow())
        if len(self.ns1['fac']) == 0:
            self.ui.remFactor1Button.setEnabled(False)
            
    def saveChangedListItem(self, item):
        if item.text() != '':
            self.ns1['fac'][item.listWidget().row(item)] = item.text()
            item.setToolTip(item.text())
        else:
            item.setText(self.ns1['fac'][item.listWidget().row(item)])

    def saveChangedTableItems(self, item):
        if item.text() != '':
            if item.column() == 0:
                self.ns1['name'] = item.text()
                item.setToolTip(item.text())
            elif item.column() == 1:
                try:
                    tmp = float(item.text().replace(',', '.'))
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                    item.setText(str(self.ns1['ver']))
                else:
                    if (tmp > 1) or (tmp < 0):
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                        item.setText(str(self.ns1['ver']))
                    else:
                        self.ns1['ver'] = tmp
        else:
            if item.column() == 0:
                item.setText(self.ns1['name'])
            else:
                item.setText(str(self.ns1['ver']))

    def save(self):
        if self.ui.factor1List.count() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список факторов пуст')
            return 1
            
        self.out = self.ns1
        self.close()
