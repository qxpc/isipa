# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././ask_weight.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Ask_weight(object):
    def setupUi(self, Ask_weight):
        Ask_weight.setObjectName("Ask_weight")
        Ask_weight.resize(764, 452)
        self.gridLayout = QtWidgets.QGridLayout(Ask_weight)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.saveButton = QtWidgets.QPushButton(Ask_weight)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(Ask_weight)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.frame3 = QtWidgets.QFrame(Ask_weight)
        self.frame3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame3.setObjectName("frame3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.frame3)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_7 = QtWidgets.QLabel(self.frame3)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_5.addWidget(self.label_7)
        self.weightTable = QtWidgets.QTableWidget(self.frame3)
        self.weightTable.setColumnCount(2)
        self.weightTable.setObjectName("weightTable")
        self.weightTable.setRowCount(0)
        self.verticalLayout_5.addWidget(self.weightTable)
        self.gridLayout.addWidget(self.frame3, 0, 0, 1, 1)

        self.retranslateUi(Ask_weight)
        QtCore.QMetaObject.connectSlotsByName(Ask_weight)

    def retranslateUi(self, Ask_weight):
        _translate = QtCore.QCoreApplication.translate
        Ask_weight.setWindowTitle(_translate("Ask_weight", "Dialog"))
        self.saveButton.setText(_translate("Ask_weight", "Сохранить"))
        self.cancelButton.setText(_translate("Ask_weight", "Отмена"))
        self.label_7.setText(_translate("Ask_weight", "Коэффициенты результативности ЗМ:"))

