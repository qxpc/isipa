# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QMessageBox
from ui_newstandartdialog import Ui_NewStandartDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

class NewStandartDialog(QDialog):
    def __init__(self):
        super(NewStandartDialog, self).__init__()
        self.ui = Ui_NewStandartDialog()
        self.ui.setupUi(self)
        self.setWindowTitle('Добавить стандарт')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.ui.buttonBox.accepted.connect(self.accept)
        self.ui.buttonBox.rejected.connect(self.reject)

    def accept(self):
        if not self.ui.name.text():
            print(self.ui.name.text())
            QMessageBox.critical(self, 'Ошибка', 'Не введено имя нового стандарта.')
        self.value = self.ui.name.text()
        self.done(0)

    def reject(self):
        self.value = ''
        self.done(-1)

    def getName(self):
        return self.value
        
            
