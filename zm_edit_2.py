from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QHeaderView, QMessageBox
from PyQt5.QtGui import QDoubleValidator, QIcon
from PyQt5.QtCore import Qt
from ui_zm_edit_2 import Ui_ZmEdit2
import copy
import json

class ZmEdit2(QDialog):
    def __init__(self):
        super(ZmEdit2, self).__init__()
        self.ui = Ui_ZmEdit2()
        self.ui.setupUi(self)
        self.setWindowTitle('Редактирование: Защитная мера 1-го уровня')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.setModal(True)
        self.out = None
        
        try:
            f = open('configs/inc.json', 'r', encoding='utf8')
            self.incfile = json.loads(f.read())
            f.close()
        except Exception:
            self.incfile = []
        
        self.ui.subTable.setColumnCount(2)
        self.ui.subTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.subTable.verticalHeader().setVisible(False)
        self.ui.subTable.horizontalHeader().setVisible(False)
        self.ui.subTable.setSelectionMode(1)
        
        self.ui.incTable.setColumnCount(2)
        self.ui.incTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.incTable.verticalHeader().setVisible(False)
        self.ui.incTable.horizontalHeader().setVisible(False)
        self.ui.incTable.setSelectionMode(1)
        
        self.ui.subTable.itemChanged.connect(self.recordValue)
        self.ui.incTable.itemChanged.connect(self.recordIncValue)
        self.ui.addButton.clicked.connect(self.addSub)
        self.ui.removeButton.clicked.connect(self.removeSub)
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)

    def showDialog(self, reqIn):
        self.req = copy.deepcopy(reqIn)
        self.ui.selectedMeasure.setText(reqIn['req'])
        self.fillTables()
        self.exec_()

    def fillTables(self):
        self.ui.subTable.clearContents()
        self.ui.subTable.setRowCount(len(self.req['sub']))
        self.ui.incTable.clearContents()
        self.ui.incTable.setRowCount(len(self.incfile))
        
        if len(self.req['sub']) == 0:
            self.ui.removeButton.setEnabled(False)
        else:
            self.ui.removeButton.setEnabled(True)
        for i, j in enumerate(self.req['sub']):
            self.ui.subTable.setItem(i, 0, QTableWidgetItem(j['name']))
            self.ui.subTable.setItem(i, 1, QTableWidgetItem(str(j['wt'])))
            self.ui.subTable.setCurrentCell(0, 0)
            
        self.wtInc = []
        for i, nameInc in enumerate(self.incfile):
            self.ui.incTable.setItem(i, 0, QTableWidgetItem(nameInc['inc']))
            wt = ''
            for j, incSub in enumerate(self.req['inc']):
                if nameInc['inc'] == incSub['name']:
                    wt = str(incSub['wt'])
                    break
            self.wtInc.append({'name': nameInc['inc'], 'wt': wt})
            self.ui.incTable.setItem(i, 1, QTableWidgetItem(wt))

            self.ui.incTable.item(i, 0).setFlags(Qt.ItemIsEnabled)
            #self.ui.incTable.item(i, 1).setFlags(Qt.ItemIsEnabled)
            self.ui.incTable.setCurrentCell(0, 0)
        if len(self.ui.incTable.findItems("-", Qt.MatchExactly)) != 0:
            self.ui.saveButton.setEnabled(False)

    def addSub(self):
        count = self.ui.subTable.rowCount()
        self.ui.subTable.insertRow(count)
        self.ui.subTable.setCurrentCell(count, 0)
        self.req['sub'].append({'name': '<Защитная мера 2-го уровня>', 'wt': 0})
        self.ui.subTable.setItem(count, 0, QTableWidgetItem('<Защитная мера 2-го уровня>'))
        self.ui.subTable.setItem(count, 1, QTableWidgetItem('0'))
        self.ui.removeButton.setEnabled(True)
        
    def recordValue(self, item):
        if item.column() == 0:
            self.req['sub'][item.row()]['name'] = item.text()
        else:
            self.req['sub'][item.row()]['wt'] = float(item.text().replace(',', '.'))

    def recordIncValue(self, item):
        if item.column() == 1:
            if item.text() != '':
                try:
                    tmp = int(item.text())
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть целое число в интервале от 0 до 4")
                    item.setText(self.wtInc[item.row()]['wt'])
                else:
                    if tmp >= 0 and tmp <= 4:
                        self.wtInc[item.row()]['wt'] = item.text()
                    else:
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть целое число в интервале от 0 до 4")
                        item.setText(self.wtInc[item.row()]['wt'])
            else:
                item.setText(self.wtInc[item.row()]['wt'])

    def removeSub(self):
        current = self.ui.subTable.currentRow()
        if self.ui.subTable.rowCount() > 0:
            del self.req['sub'][current]
            self.ui.subTable.removeRow(current)
        if len(self.req['sub']) == 0:
            self.ui.removeButton.setEnabled(False)
            
    def save(self):
        if self.ui.selectedMeasure.text() == '':
            QMessageBox.critical(self, 'Ошибка', 'Укажите название защитной меры 1-го уровня')
            self.ui.selectedMeasure.setFocus(3)
            return 1
        if self.ui.subTable.rowCount() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Перечень защитных мер 2-го уровня пуст')
            return 1
        
        for j, i in enumerate(self.wtInc):
            if i['wt'] == '':
                QMessageBox.critical(self, 'Ошибка', 'Укажите вес инцидента')
                self.ui.incTable.setFocus(3)
                self.ui.incTable.setCurrentCell(j, 1)
                return 1
        for i in self.wtInc:
            i['wt'] = int(i['wt'])
        
        self.req['req'] = self.ui.selectedMeasure.text()
        self.req['inc'] = copy.deepcopy(self.wtInc)
            
        self.out = self.req
        self.close()
