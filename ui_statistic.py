# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././statistic.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Statistic(object):
    def setupUi(self, Statistic):
        Statistic.setObjectName("Statistic")
        Statistic.resize(409, 516)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Statistic.sizePolicy().hasHeightForWidth())
        Statistic.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(Statistic)
        self.verticalLayout.setObjectName("verticalLayout")
        self.splitter = QtWidgets.QSplitter(Statistic)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.table1 = QtWidgets.QTableWidget(self.splitter)
        self.table1.setObjectName("table1")
        self.table1.setColumnCount(0)
        self.table1.setRowCount(0)
        self.verticalLayout.addWidget(self.splitter)
        self.line = QtWidgets.QFrame(Statistic)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.splitter_3 = QtWidgets.QSplitter(Statistic)
        self.splitter_3.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_3.setObjectName("splitter_3")
        self.layoutWidget = QtWidgets.QWidget(self.splitter_3)
        self.layoutWidget.setObjectName("layoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.layoutWidget)
        self.formLayout.setLabelAlignment(QtCore.Qt.AlignCenter)
        self.formLayout.setFormAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label_2)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.label_4)
        self.label_5 = QtWidgets.QLabel(self.layoutWidget)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.label_6 = QtWidgets.QLabel(self.layoutWidget)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.label_6)
        self.verticalLayout.addWidget(self.splitter_3)
        self.line_2 = QtWidgets.QFrame(Statistic)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.splitter_2 = QtWidgets.QSplitter(Statistic)
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName("splitter_2")
        self.table2 = QtWidgets.QTableWidget(self.splitter_2)
        self.table2.setObjectName("table2")
        self.table2.setColumnCount(0)
        self.table2.setRowCount(0)
        self.verticalLayout.addWidget(self.splitter_2)

        self.retranslateUi(Statistic)
        QtCore.QMetaObject.connectSlotsByName(Statistic)

    def retranslateUi(self, Statistic):
        _translate = QtCore.QCoreApplication.translate
        Statistic.setWindowTitle(_translate("Statistic", "Dialog"))
        self.label.setText(_translate("Statistic", "Коэффициент дефицита персонала"))
        self.label_2.setText(_translate("Statistic", "TextLabel"))
        self.label_3.setText(_translate("Statistic", "Зрелость процесса мнеджмента\n"
"непрерывности бизнеса"))
        self.label_4.setText(_translate("Statistic", "TextLabel"))
        self.label_5.setText(_translate("Statistic", "Зрелость процесса мнеджмента\n"
"инцидентов ИБ"))
        self.label_6.setText(_translate("Statistic", "TextLabel"))

