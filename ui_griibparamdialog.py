# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././griibparamdialog.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GriibParamDialog(object):
    def setupUi(self, GriibParamDialog):
        GriibParamDialog.setObjectName("GriibParamDialog")
        GriibParamDialog.resize(602, 500)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(125)
        sizePolicy.setHeightForWidth(GriibParamDialog.sizePolicy().hasHeightForWidth())
        GriibParamDialog.setSizePolicy(sizePolicy)
        GriibParamDialog.setMaximumSize(QtCore.QSize(16777215, 500))
        self.gridLayout = QtWidgets.QGridLayout(GriibParamDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.radioButton = QtWidgets.QRadioButton(GriibParamDialog)
        self.radioButton.setObjectName("radioButton")
        self.verticalLayout.addWidget(self.radioButton)
        self.radioButton_2 = QtWidgets.QRadioButton(GriibParamDialog)
        self.radioButton_2.setObjectName("radioButton_2")
        self.verticalLayout.addWidget(self.radioButton_2)
        self.radioButton_3 = QtWidgets.QRadioButton(GriibParamDialog)
        self.radioButton_3.setObjectName("radioButton_3")
        self.verticalLayout.addWidget(self.radioButton_3)
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(104, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.okButton = QtWidgets.QPushButton(GriibParamDialog)
        self.okButton.setObjectName("okButton")
        self.horizontalLayout.addWidget(self.okButton)
        spacerItem1 = QtWidgets.QSpacerItem(103, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.textBrowser = QtWidgets.QTextBrowser(GriibParamDialog)
        self.textBrowser.setObjectName("textBrowser")
        self.gridLayout.addWidget(self.textBrowser, 2, 0, 1, 1)

        self.retranslateUi(GriibParamDialog)
        QtCore.QMetaObject.connectSlotsByName(GriibParamDialog)

    def retranslateUi(self, GriibParamDialog):
        _translate = QtCore.QCoreApplication.translate
        GriibParamDialog.setWindowTitle(_translate("GriibParamDialog", "Dialog"))
        self.radioButton.setText(_translate("GriibParamDialog", "Начальный уровень - ситуационное реагирование"))
        self.radioButton_2.setText(_translate("GriibParamDialog", "Средний уровень - центр реагирования только создан"))
        self.radioButton_3.setText(_translate("GriibParamDialog", "Высокий уровень - центр реагирования занимает ключевое место в системе ИБ"))
        self.okButton.setText(_translate("GriibParamDialog", "OK"))

