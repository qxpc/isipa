#!/usr/bin/env python3
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QTableWidgetItem, QFileDialog, QCheckBox, QComboBox
from PyQt5.QtCore import Qt, QMimeData, QSignalMapper
from PyQt5.QtGui import QIcon
from ui_mainwindow import Ui_MainWindow
from imitmodel import ImitModel
from measuresdialog import MeasuresDialog, ReadFileJson
from zm_edit_0 import ZmEdit
from outputdialog import OutputDialog
from edit_inc_0 import Edit_inc_0
from newstandartdialog import NewStandartDialog
from griibparamdialog import GriibParamDialog
from incidentprobabilities import IncidentProbabilities
import json, binascii, random, os, math, random, copy


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.mdialog = MeasuresDialog()
        home = os.getcwd()
        
        self.setWindowTitle('Information Security Incidents Preparedness Assessment (ISIPA)')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.inc = ReadFileJson.readFileJson(self, 'configs/inc.json')
        self.ui.tableFactors.setColumnCount(3)
        self.ui.tableFactors.horizontalHeader().hide()
        self.ui.tableFactors.verticalHeader().hide()
        
        """Переменные для формул"""
        self.SPrime = float(0)
        self.P_inc = [] #P_и
        self.P_inc_zm = [] #P_и^зм
        self.R_p = 0 #R_п
        self.R_e = 0 #R_э
        self.R_am = 0 #R_ам
        self.t_esk = 0 #время эскалации
        self.K_dp = float(2)
        self.lamb = [] #Вес инцидента
        self.m = [0.3, 0.15, 0.1, 0.15, 0.3] #Результат обработки
        self.t_pr_sder = []  # примерное время сдерживания
        self.t_pr_ustr = []  # примерное время устранения
        self.t_pr_vosst = [] # примерное время восстановления
        self.counter = []
        self.experiments = []
        self.resObrab = []
        
        """получение списка стандартов"""
        self.dir = './configs/standarts'
        try:
            standarts = os.listdir(self.dir) 
            for i in standarts:
                if (i.endswith('.json') == False):
                    standarts.remove(i)
            if len(standarts) == 0: self.ui.pushProtectMeasures.setEnabled(False)
            for i in range(len(standarts)):
                standarts[i] = standarts[i].rstrip(".json")
            self.ui.boxStandarts.addItems(standarts)
            self.openStandart()
        except FileNotFoundError:
            os.makedirs(self.dir)
            self.require = []
        
        self.ui.pushSaveSettings.clicked.connect(self.showOutput) #формулы
        self.ui.pushAdd.clicked.connect(self.addStandart)
        self.ui.pushRemove.clicked.connect(self.delStandart)
        self.ui.boxStandarts.currentTextChanged.connect(self.openStandart)
        self.ui.incEditButton.clicked.connect(self.showIncidDialog)
        self.ui.pushSetAllFactors.clicked.connect(self.checkAllFactors)
        self.ui.buttonRandomSelection.clicked.connect(self.randomSelection)
        self.ui.pushUnsetAllFactors.clicked.connect(self.uncheckAllFactors)
        self.ui.pushProtectMeasures.clicked.connect(self.showMDialog)
        self.ui.pushModify.clicked.connect(self.modifyStandart)
        self.ui.parGriibButton.clicked.connect(self.getGriibParam)
        self.ui.scaleButton.clicked.connect(self.getLambAndProcRes)
        self.ui.imitModelButton.clicked.connect(self.showImitModel)
        
        self.fillFactorList()
    
    def showMDialog(self):
        if self.checkInc() == 0:
            self.mdialog.show()
    
    def openStandart(self):
        self.name = self.ui.boxStandarts.currentText()
        if self.name != '':
            self.require = ReadFileJson.readFileJson(self, self.dir + '/' + self.name + '.json')
            if self.mdialog.openCustomJsonFile(self.require) == 0:
                self.buttonSetEnabledReq(True)
            else:
                QMessageBox.critical(self, "Ошибка!", "Ошибка в файле стандарта")
                self.buttonSetEnabledReq(False)
                self.require = []
        else:
            self.buttonSetEnabledReq(False)
            self.require = []
            
    def buttonSetEnabledReq(self, status):
        self.ui.pushModify.setEnabled(status)
        self.ui.pushRemove.setEnabled(status)
        self.ui.pushProtectMeasures.setEnabled(status)
        self.ui.pushSaveSettings.setEnabled(status)
        self.ui.imitModelButton.setEnabled(status)

        self.ui.parGriibButton.setEnabled(status)
        self.ui.scaleButton.setEnabled(status)
        self.ui.pushSaveSettings.setEnabled(status)
        self.ui.imitModelButton.setEnabled(status)
        self.ui.incEditButton.setEnabled(status)
        if status == True and self.inc == []:
            self.buttonSetEnabledInc(False)
    def addStandart(self):
        newStandart = NewStandartDialog()
        newStandart.exec_()
        newName = newStandart.getName()
        if newName:           
            if newName + '.json' in os.listdir(self.dir):
                QMessageBox.critical(self, "Ошибка!", "Такой файл уже существует")
            else:
                self.require = []
                self.name = newName
                self.modifyStandart()
                if ((newName + '.json') in os.listdir(self.dir)): #в список добавляется только при нажатии "готово"
                    self.buttonSetEnabledReq(True)
                    self.ui.boxStandarts.addItem(newName)
                    self.ui.boxStandarts.setCurrentIndex(self.ui.boxStandarts.findText(newName))
                    
        
    def delStandart(self):
        if (self.ui.boxStandarts.count() > 0):
            #name = self.ui.boxStandarts.currentText()
            os.remove(self.dir + '/' + self.name + '.json') 
            self.ui.boxStandarts.removeItem(self.ui.boxStandarts.currentIndex())

    def fillFactorList(self):
        try:
            if self.inc != []:
                mapper = QSignalMapper(self)
                
                self.lamb = [[i['inc'], 1/len(self.inc)] for i in self.inc]
                self.ui.tableFactors.verticalScrollBar().setValue(0) # скроллинг
                self.ui.tableFactors.clearContents()
                
                self.checkList = []
                listFactorItem = list()
                for i in self.inc:
                    for j in i['ns2']:
                        listFactorItem.extend(j['fac'])
                        for k in j['ns1']:
                            listFactorItem.extend(k['fac'])
                            
                listFactorItem = list(set(listFactorItem))
                listFactorItem.sort()
                self.ui.tableFactors.setRowCount(len(listFactorItem))
                for i, j in enumerate(listFactorItem):
                    self.ui.tableFactors.setCellWidget(i, 0, QCheckBox())
                    checkBox = self.ui.tableFactors.cellWidget(i, 0)
                    checkBox.stateChanged.connect(mapper.map)
                    mapper.setMapping(checkBox, i)
                    
                    self.ui.tableFactors.setCellWidget(i, 1, QComboBox())
                    self.ui.tableFactors.cellWidget(i, 1).addItems(['1', '2', '3', '4', '5', 'не известно'])
                    self.ui.tableFactors.cellWidget(i, 1).setCurrentIndex(5)
                    self.ui.tableFactors.cellWidget(i, 1).setEnabled(False)
                    
                    widget = QTableWidgetItem(j)
                    widget.setToolTip(widget.text())
                    self.ui.tableFactors.setItem(i, 2, widget)
                    self.ui.tableFactors.item(i, 2).setFlags(Qt.ItemIsEnabled)
                    
                mapper.mapped[int].connect(self.changedCheckBox)
                self.ui.tableFactors.resizeColumnToContents(0)
                self.ui.tableFactors.horizontalHeader().setStretchLastSection(True)
                
                self.mdialog.ui.comboActiveValue.clear()
                # for i in range(len( self.inc[0]['matrix'][0] )):
                    # self.mdialog.ui.comboActiveValue.addItem(str(i + 1))
                self.mdialog.ui.comboActiveValue.addItems(["Высокая", "Средняя", "Низкая"]) # изменено
                self.buttonSetEnabledInc(True)
            else:
                QMessageBox.critical(self, "Ошибка!", "Файл с видами инцидентов пуст или отсутствует")
                self.buttonSetEnabledInc(False)
        except Exception:
            QMessageBox.critical(self, "Ошибка!", "Файл с видами инцидентов пуст или отсутствует")
            print(self.inc)
            self.inc = []
            self.buttonSetEnabledInc(False)
            
    def buttonSetEnabledInc(self, status):
        self.ui.pushSetAllFactors.setEnabled(status)
        self.ui.buttonRandomSelection.setEnabled(status)
        self.ui.pushUnsetAllFactors.setEnabled(status)
        self.ui.pushProtectMeasures.setEnabled(status)
        self.ui.parGriibButton.setEnabled(status)
        self.ui.scaleButton.setEnabled(status)
        self.ui.pushSaveSettings.setEnabled(status)
        self.ui.imitModelButton.setEnabled(status)

    def checkAllFactors(self):
        for i in range(self.ui.tableFactors.rowCount()):
            self.ui.tableFactors.cellWidget(i, 0).setCheckState(Qt.Checked)
                
    def randomSelection(self):
        for i in range(self.ui.tableFactors.rowCount()):
            self.ui.tableFactors.cellWidget(i, 0).setCheckState(Qt.Unchecked)
            
        for i in range(random.randint(1, self.ui.tableFactors.rowCount() - 1)):
            self.ui.tableFactors.cellWidget(random.randint(0, self.ui.tableFactors.rowCount() - 1), 0).setCheckState(Qt.Checked)

    def uncheckAllFactors(self):
        for i in range(self.ui.tableFactors.rowCount()):
            self.ui.tableFactors.cellWidget(i, 0).setCheckState(Qt.Unchecked)

    def modifyStandart(self):
        self.showMeasuresDialog()
        self.openStandart()
        
    def showMeasuresDialog(self):
        zmEditDialog = ZmEdit()
        zmEditDialog.setReq(self.require)
        zmEditDialog.fillSection()
        zmEditDialog.exec_()
        
        if zmEditDialog.out is not None:
            self.require = copy.deepcopy(zmEditDialog.out)
            
            with open('configs/standarts/' + self.name + '.json', 'w', encoding='utf8') as output:
                json.dump(self.require, output, ensure_ascii = False, indent=4)
    
    def showIncidDialog(self):
        incid = Edit_inc_0()
        incid.update(self.inc, self.require)
        incid.exec_()
        
        if incid.outInc is not None:
            self.inc = copy.deepcopy(incid.outInc)
            self.fillFactorList()
            self.require = copy.deepcopy(incid.outReq)
            
            with open('configs/inc.json', 'w', encoding='utf8') as output:
                json.dump(self.inc, output, ensure_ascii = False, indent=4)
            
            with open('configs/standarts/' + self.name + '.json', 'w', encoding='utf8') as output:
                json.dump(self.require, output, ensure_ascii = False, indent=4)
            self.openStandart()
    
    def checkDict(self, incList, nameInc):
        b = False
        for i in incList:
            if (i['name'] == nameInc) and (i['wt'] == 4 or \
            i['wt'] == 3 or i['wt'] == 2 or i['wt'] == 1 or i['wt'] == 0):
                b = True
                break
        return b
    
    def checkInc(self):
        for i in self.inc:
            for j in self.require:
                for k in j["reqList"]:
                    if self.checkDict(k['inc'], i['inc']) == False:
                        QMessageBox.critical(self, 'Ошибка', 'Указаны не все коэффициенты результативности ЗМ')
                        return 1
        return 0
     
    def formulas(self):
        '''формулы'''
        if self.checkInc() == 1:
            return -1
            
        selItems = list()
        for i in range(self.ui.tableFactors.rowCount()):
            if self.ui.tableFactors.cellWidget(i, 0).checkState():
                selItems.append(self.ui.tableFactors.item(i, 2).text())
        
        if selItems == []:
            QMessageBox.critical(self, 'Ошибка', 'Выберите факторы')
            return -1
        
        self.P_inc.clear()
        #цикл по инцидентам
        for inc in self.inc:
            self.P_inc.append(1)
            for ns2 in inc['ns2']:
                P_j = 0
                if set(ns2['fac']) & set(selItems):
                    P_j = 1
                else:
                    P_j = 1
                    for ns1 in ns2['ns1']:
                        if set(ns1['fac']) & set(selItems):
                            P_j *= 1 - float(ns1['ver'])
                    P_j = 1 - P_j
                self.P_inc[-1] *= 1 - P_j
                
            self.P_inc[-1] = 1 - self.P_inc[-1]
            
        
        self.selInc = set()
        for inc in self.inc:
            for ns2 in inc['ns2']:
                if set(ns2['fac']) & set(selItems):
                    self.selInc.add(inc['inc'])
                    break
                for ns1 in ns2['ns1']:
                    if set(ns1['fac']) & set(selItems):
                        self.selInc.add(inc['inc'])
                        break
        
        self.R_p = self.mdialog.getR_p()
        
        """Формулы2"""       
        self.P_inc_zm.clear()
        for i in range(len(self.P_inc)):
            j = self.inc[i]['inc']
            self.P_inc_zm.append(float(self.P_inc[i] * (1 - self.R_p[j])))
                
        
        self.R_e = self.mdialog.getR_e()
        self.R_am = self.mdialog.getR_am()
        
    def expVal(self, l):
        '''Подсчет мат ожидания '''
        sum = 0
        for i in l:
            sum += i['time'] / len(l)
        return sum

    def minDict(self, l):
        min = l[0]['time']
        for i in l:
            if min > i['time']:
                min = i['time']
        return min
    
    def formulas2(self):
        """Формулы рисунок 4"""
        self.t_pr_sder.clear()
        self.t_pr_ustr.clear()
        self.t_pr_vosst.clear()
        for i in self.inc:
            self.t_pr_sder.append( self.expVal(i['tmin'][0]) * self.K_dp / self.R_e[i['inc']])
            self.t_pr_ustr.append( self.expVal(i['tmin'][1]) * self.K_dp / self.R_e[i['inc']])
            self.t_pr_vosst.append( self.expVal(i['tmin'][2]) * self.K_dp / self.R_e[i['inc']])
        # print(self.t_pr_sder)
        # print(self.t_pr_ustr)
        # print(self.t_pr_vosst)
        # """Вывод результата"""
        # output = OutputDialog()
        # output.fill(self.inc, self.R_p, self.R_e, self.P_inc, self.P_inc_zm)
        # output.exec_()
        # QMessageBox.information(self, "Результат расчета", 
        # "Вероятность инцидентов: " + str(self.P_inc) +
        # "\nЭффективность процедур: " + str(self.R_p) +
        # "\nЭффективность процедур реагирования: " + str(self.R_e) +
        # "\nВероятность инцидентов с учетом ЗМ: " + str(self.P_inc_zm) +
        # "\nВремя эскалации: " + str(self.t_esk) +
        # "\nВремя сдерживания" + str(self.t_pr_sder) +
        # "\nВремя устранения" + str(self.t_pr_ustr) +
        # "\nВремя восстановления" + str(self.t_pr_vosst))
                
        """формулы из почты"""
        self.counter = []
        exper = self.mdialog.getNumExperiments() # количество опытов
        self.experiments.clear()
        self.resObrab.clear()
       
        
        for i, incn in enumerate(self.inc):
            self.experiments.append([])
            self.resObrab.append([])
            self.counter.append([0, 0, 0, 0, 0, incn['inc']])
            for e in range(exper):
                self.experiments[-1].append([])
                xi_1 = self.rand()         
                if xi_1 >= self.P_inc_zm[i]:
                    ##print("инцидент предотвращен")
                    self.counter[i][0] += 1
                    self.resObrab[-1].append('Инцидент предотвращен')
                    self.experiments[-1][-1].append('-')
                    self.experiments[-1][-1].append('-')
                    self.experiments[-1][-1].append('-')
                    self.experiments[-1][-1].append('-')
                else:
                    #3)
                    row = random.randint(0, 4) # xi_2
                    if len(self.checkList) == 1:
                        if self.ui.tableFactors.cellWidget(self.checkList[0], 1).currentIndex() != 5: 
                            row = self.ui.tableFactors.cellWidget(self.checkList[0], 1).currentIndex()
                    column = self.mdialog.getActiveValue()
                    t_esk = round(incn['matrix'][row][column] * self.mdialog.getMinTimeToEvent(), 2)
                    self.experiments[-1][-1].append(t_esk)
                                    
                    # время сдерживания
                    t_sder = round(self.minDict(incn['tmin'][0]) - math.log(1 - self.rand()) * self.t_pr_sder[i], 2)
                    self.experiments[-1][-1].append(t_sder)
                    
                    if t_sder < t_esk:
                        #6)
                        t_ustr = round(self.minDict(incn['tmin'][1]) - math.log(1 - self.rand()) * self.t_pr_ustr[i], 2)
                        t_vosst = round(self.minDict(incn['tmin'][2]) - math.log(1 - self.rand()) * self.t_pr_vosst[i], 2)
                        self.experiments[-1][-1].append(t_ustr)
                        self.experiments[-1][-1].append(t_vosst)
                        #print(self.t_pr_sder[i], t_vosst, t_sder, t_ustr, t_esk)
                        if t_vosst + t_sder + t_ustr < t_esk:
                            self.counter[i][1] += 1
                            self.resObrab[-1].append('Инцидент успешно обработан')
                            #print("обработано")
                        else: 
                            self.counter[i][2] += 1
                            self.resObrab[-1].append('Инцидент обработан с незначительными последствиями')
                    else:
                        #7)
                        self.experiments[-1][-1].append('-')
                        self.experiments[-1][-1].append('-')
                        xi_3 = self.rand()
                        if xi_3 < self.R_am[incn['inc']]:
                            self.counter[i][3] += 1
                            self.resObrab[-1].append('Инцидент обработан со значительными последствиями')
                        else:
                            self.counter[i][4] += 1
                            self.resObrab[-1].append('Произошла эскалация инцидента')
        
        c = 0
        while c < len(self.counter):
            if not self.counter[c][-1] in self.selInc:
                self.counter.pop(c)
                self.experiments.pop(c)
                self.resObrab.pop(c)
            else:
                c += 1

        for i in range(len(self.counter)):
            for j in range(len(self.counter[i][:-1])):
                self.counter[i][j] /= exper
        
        # for i, j in enumerate(self.counter):            
            # if (float(j[1]) == 1) and (float(self.P_inc[i]) == 0):
                # for k in range(len(j)[:-1]):
                    # self.counter[i][k] = '-'
        
        for i in self.experiments:
            for e in i:
                sum = 0
                b = False
                for k in e:
                    if k != '-':
                        sum += k
                        b = True
                if b == True:
                    e.append(round(sum, 2))
                else:
                    e.append('-')
        
    def rand(self): # возвращает случайное число [0, 1)
        xi = 1
        while xi == 1:
            xi = random.random()
        return xi
                
    def changedCheckBox(self, row):
        table = self.ui.tableFactors
        if table.cellWidget(row, 0).checkState() == 2:
            self.checkList.append(row)
            if len(self.checkList) == 2:
                table.cellWidget(self.checkList[0], 1).setCurrentIndex(5)
                table.cellWidget(self.checkList[0], 1).setEnabled(False)
        else:
            if len(self.checkList) == 1:
                table.cellWidget(self.checkList[0], 1).setCurrentIndex(5)
                table.cellWidget(self.checkList[0], 1).setEnabled(False)
            self.checkList.remove(row)
            
        if len(self.checkList) == 1:
            table.cellWidget(self.checkList[0], 1).setEnabled(True)
        

 
    def getCheckedFactors(self, x):
        if x.checkState() == Qt.Checked:
            return True
        else:
            return False

    def getGriibParam(self):
        griibDialog = GriibParamDialog()
        griibDialog.setCoef(self.K_dp)
        griibDialog.exec_()
        self.K_dp = float(griibDialog.getCoef())

    def getLambAndProcRes(self):
        incProb = IncidentProbabilities(self.lamb, self.m, self.SPrime)
        incProb.exec_()
        if incProb.out1 != []:
            self.lamb = copy.deepcopy(incProb.out1)
            self.m = copy.deepcopy(incProb.out2)
            self.SPrime = incProb.SPrime

    def showOutput(self):
        if self.formulas() != -1:
            """Вывод результата"""
            output = OutputDialog()
            output.fill(self.inc, self.R_p, self.R_e, self.R_am, self.P_inc, self.P_inc_zm)
            output.exec_()
            
    def showImitModel(self):
        if self.formulas() != -1:
            if self.formulas2() != -1:
                imitModel = ImitModel(self.lamb, self.m, self.counter, self.experiments, self.resObrab, self.SPrime)
                imitModel.exec_()
