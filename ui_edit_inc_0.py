# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././edit_inc_0.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Edit_inc_0(object):
    def setupUi(self, Edit_inc_0):
        Edit_inc_0.setObjectName("Edit_inc_0")
        Edit_inc_0.resize(764, 452)
        self.gridLayout = QtWidgets.QGridLayout(Edit_inc_0)
        self.gridLayout.setObjectName("gridLayout")
        self.frame1 = QtWidgets.QFrame(Edit_inc_0)
        self.frame1.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame1.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame1.setObjectName("frame1")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.frame1)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label = QtWidgets.QLabel(self.frame1)
        self.label.setMaximumSize(QtCore.QSize(16777215, 17))
        self.label.setObjectName("label")
        self.gridLayout_6.addWidget(self.label, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.incList = QtWidgets.QListWidget(self.frame1)
        self.incList.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.incList.setObjectName("incList")
        self.horizontalLayout_2.addWidget(self.incList)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.addIncButton = QtWidgets.QPushButton(self.frame1)
        self.addIncButton.setObjectName("addIncButton")
        self.verticalLayout.addWidget(self.addIncButton)
        self.remIncButton = QtWidgets.QPushButton(self.frame1)
        self.remIncButton.setObjectName("remIncButton")
        self.verticalLayout.addWidget(self.remIncButton)
        self.editIncButton = QtWidgets.QPushButton(self.frame1)
        self.editIncButton.setObjectName("editIncButton")
        self.verticalLayout.addWidget(self.editIncButton)
        self.editWghtButton = QtWidgets.QPushButton(self.frame1)
        self.editWghtButton.setObjectName("editWghtButton")
        self.verticalLayout.addWidget(self.editWghtButton)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.gridLayout_6.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.frame1, 0, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.saveButton = QtWidgets.QPushButton(Edit_inc_0)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(Edit_inc_0)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)

        self.retranslateUi(Edit_inc_0)
        QtCore.QMetaObject.connectSlotsByName(Edit_inc_0)

    def retranslateUi(self, Edit_inc_0):
        _translate = QtCore.QCoreApplication.translate
        Edit_inc_0.setWindowTitle(_translate("Edit_inc_0", "Dialog"))
        self.label.setText(_translate("Edit_inc_0", "Виды инцидентов:"))
        self.addIncButton.setText(_translate("Edit_inc_0", "Добавить..."))
        self.remIncButton.setText(_translate("Edit_inc_0", "Удалить"))
        self.editIncButton.setText(_translate("Edit_inc_0", "Изменить..."))
        self.editWghtButton.setText(_translate("Edit_inc_0", "Именить коэфф.\n"
"результативности\n"
"ЗМ..."))
        self.saveButton.setText(_translate("Edit_inc_0", "Сохранить"))
        self.cancelButton.setText(_translate("Edit_inc_0", "Отмена"))

