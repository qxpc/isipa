# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_ask_weight import Ui_Ask_weight
from PyQt5.QtCore import Qt
import copy

class Ask_weight(QDialog):
    def __init__(self):
        super(Ask_weight, self).__init__()
        self.ui = Ui_Ask_weight()
        self.ui.setupUi(self)
        self.out = None
        self.setWindowTitle('Редактирование: Коэффициенты результативности ЗМ')
        self.setWindowIcon(QIcon('resources/icon.png'))
        
        self.ui.weightTable.setColumnCount(2)
        self.ui.weightTable.setSelectionMode(1)
        self.ui.weightTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.weightTable.verticalHeader().setVisible(False)
        self.ui.weightTable.horizontalHeader().setVisible(False)
     
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        
    def update(self, req, str):
        self.req = copy.deepcopy(req)
        self.nameInc = str
        self.fillWeightTable()
        
    def fillWeightTable(self):
        self.ui.weightTable.clearContents()
        self.ui.weightTable.setRowCount(0)
        
        for i in self.req:
            for k in i['reqList']:
                self.ui.weightTable.insertRow(self.ui.weightTable.rowCount())
                self.ui.weightTable.setItem(self.ui.weightTable.rowCount() - 1, 0, QTableWidgetItem(k['req']))
                self.ui.weightTable.item(self.ui.weightTable.rowCount() - 1, 0).setFlags(Qt.ItemIsEnabled)
                for j in range(0, len(k['inc'])):
                    if k['inc'][j]['name'] == self.nameInc:
                        self.ui.weightTable.setItem(self.ui.weightTable.rowCount() - 1, 1, QTableWidgetItem(str(k['inc'][j]['wt'])))
                        k['inc'][j]['wt'] = 0
                        break

    def save(self):
        w = 0
        for i in self.req:
            for k in i['reqList']:
                try:
                    tmp = int(self.ui.weightTable.item(w, 1).text())
                    if tmp < 0 or tmp > 4:
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть целое число в интервале от 0 до 4")
                        self.ui.weightTable.setCurrentCell(w, 1)
                        self.ui.weightTable.setFocus(3)
                        return 1
                except Exception:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть целое число в интервале от 0 до 4")
                    self.ui.weightTable.setCurrentCell(w, 1)
                    self.ui.weightTable.setFocus(3)
                    return 1
                
                try:
                    index = k['inc'].index({'name': self.nameInc, 'wt': 0})
                    k['inc'][index]['wt'] = tmp
                except ValueError:
                    k['inc'].append({'name': self.nameInc, 'wt': tmp}) 
                w += 1
        self.out = self.req
        self.close()
