# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QTreeWidgetItem, QMessageBox, QFileDialog
from ui_measuresdialog import Ui_MeasuresDialog
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QIcon
from PyQt5.QtCore import Qt
import json, random

class MeasuresDialog(QDialog):
    def __init__(self):
        super(MeasuresDialog, self).__init__()
        self.ui = Ui_MeasuresDialog()
        self.ui.setupUi(self)
        #self.setParamSettings()
        self.setWindowTitle("Выбор защитных мер")
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.setModal(True)
        #self.ui.pushOk.clicked.connect(self.close)
        self.ui.pushOk.clicked.connect(self.clickOk) #формулы
        self.ui.pushSelectedAll.clicked.connect(self.SelectedAll)
        self.ui.pushSelectedNone.clicked.connect(self.SelectedNone)
        self.ui.pushSelectedRand.clicked.connect(self.SelectedRand)
        self.ui.spinMinTimeToEvent.setValue(1)
        

    def openCustomJsonFile(self, require):
        # path = QFileDialog.getOpenFileName(self, 'Открыть файл с требованиями', './configs', "JSON files (*.json);;All files (*.*)")[0]
        # self.require = ''
        # # os.path.abspath(os.curdir) # Абсолютный путь
        # if (path != ''):
            # tmp = ReadFileJson.readFileJson(self, path)
            # if (tmp != 'err'):
                # require = tmp
                # self.ui.linePath.setText(str(path))
                # Заполнение дерева:
        try:
            self.req = require
            self.ui.listProtectMeasures.header().close()
            self.ui.listProtectMeasures.clear()
            listMeasureItem = list()
            sublistMeasureItem = list()
            subsublistMeasureItem = list()
            for k in self.req:
                listMeasureItem.append(QTreeWidgetItem([k['sec']]))
                sublistMeasureItem.append([])
                for i in k['reqList']:
                    sublistMeasureItem[-1].append(QTreeWidgetItem([i['req']]))
                    sublistMeasureItem[-1][-1].setToolTip(0, i['req'])
                    subsublistMeasureItem.append([])
                    for l in i['sub']:
                        subsublistMeasureItem[-1].append(QTreeWidgetItem([l['name']]))
                        subsublistMeasureItem[-1][-1].setCheckState(0, Qt.Unchecked)
                        subsublistMeasureItem[-1][-1].setToolTip(0, l['name'])
                    sublistMeasureItem[-1][-1].addChildren(subsublistMeasureItem[-1])
                listMeasureItem[-1].addChildren(sublistMeasureItem[-1])
                self.ui.listProtectMeasures.addTopLevelItems(listMeasureItem)
                
            self.R_p = {c['name']: 0 for c in self.req[0]['reqList'][0]['inc']}
            self.R_e = {c['name']: 0.01 for c in self.req[0]['reqList'][0]['inc']}
            self.R_am = {c['name']: 0 for c in self.req[0]['reqList'][0]['inc']}
            
            return 0
        except Exception:
            self.ui.listProtectMeasures.clear()
            return 1
   
    def clickOk(self):
        countR_p = {(c['name']):0 for c in self.req[0]['reqList'][0]['inc']}
        countR_e = {c['name']:0 for c in self.req[0]['reqList'][0]['inc']}
        countR_am = {c['name']:0 for c in self.req[0]['reqList'][0]['inc']}
        self.R_p = {c['name']:0 for c in self.req[0]['reqList'][0]['inc']}
        self.R_e = {c['name']:0 for c in self.req[0]['reqList'][0]['inc']}
        self.R_am = {c['name']:0 for c in self.req[0]['reqList'][0]['inc']}
        
        for k in range(self.ui.listProtectMeasures.topLevelItemCount()):
            for i in range(self.ui.listProtectMeasures.topLevelItem(k).childCount()):
                tmp = 0
                for j in range(self.ui.listProtectMeasures.topLevelItem(k).child(i).childCount()):
                    t = self.ui.listProtectMeasures.topLevelItem(k).child(i).child(j)
                    if t.checkState(0) == Qt.Checked:
                        tmp += self.req[k]['reqList'][i]['sub'][j]['wt']
                if self.req[k]['sec'] == 'Менеджмент инцидентов информационной безопасности':
                    for inc in self.req[k]['reqList'][i]['inc']:
                        if inc['wt'] == 4:
                            countR_e[inc['name']] += 1
                            self.R_e[inc['name']] += tmp
                        elif inc['wt'] == 3:
                            self.R_e[inc['name']] += tmp * 0.75
                            countR_e[inc['name']] += 1
                        elif inc['wt'] == 2:
                            self.R_e[inc['name']] += tmp * 0.5
                            countR_e[inc['name']] += 1
                        elif inc['wt'] == 1:
                            self.R_e[inc['name']] += tmp * 0.25
                            countR_e[inc['name']] += 1
                        #elif self.req[k]['reqList'][i]['inc'][inc]['wt'] == 0:
                            #self.R_e[inc] += 0
                
                elif self.req[k]['sec'] == 'Менеджмент непрерывности бизнеса':
                    for inc in self.req[k]['reqList'][i]['inc']:
                        if inc['wt'] == 4:
                            countR_am[inc['name']] += 1
                            self.R_am[inc['name']] += tmp
                        elif inc['wt'] == 3:
                            self.R_am[inc['name']] += tmp * 0.75
                            countR_am[inc['name']] += 1
                        elif inc['wt'] == 2:
                            self.R_am[inc['name']] += tmp * 0.5
                            countR_am[inc['name']] += 1
                        elif inc['wt'] == 1:
                            self.R_am[inc['name']] += tmp * 0.25
                            countR_am[inc['name']] += 1
                        #elif self.req[k]['reqList'][i]['inc'][inc]['wt'] == 0:
                            #self.R_am[inc] += 0
                            
                else:
                    for inc in self.req[k]['reqList'][i]['inc']:
                        if inc['wt'] == 4:
                            countR_p[inc['name']] += 1
                            self.R_p[inc['name']] += tmp
                        elif inc['wt'] == 3:
                            self.R_p[inc['name']] += tmp * 0.75
                            countR_p[inc['name']] += 1
                        elif inc['wt'] == 2:
                            self.R_p[inc['name']] += tmp * 0.5
                            countR_p[inc['name']] += 1
                        elif inc['wt'] == 1:
                            self.R_p[inc['name']] += tmp * 0.25
                            countR_p[inc['name']] += 1
                        #elif self.req[k]['reqList'][i]['inc'][inc]['wt'] == 0:
                            #self.R_p[inc] += 0

                        
                # if self.req[k]['sec'] == 'Менеджмент инцидентов информационной безопасности':
                    # list_R_e.append(tmp)
                # elif self.req[k]['sec'] == 'Менеджмент непрерывности бизнеса':
                    # list_R_am.append(tmp)
                # else:
                    # list_R_p.append(tmp)
                    
        #print("=======================")
        #print(self.R_p)
        #print(self.R_e)
        #print(self.R_am)
        
        for i in self.R_p:
            try: 
                self.R_p[i] /= countR_p[i]
            except ZeroDivisionError:
                self.R_p[i] = 0
        
        for i in self.R_e:
            try:
                self.R_e[i] /= countR_e[i]
            except ZeroDivisionError:
                self.R_e[i] = float(self.ui.spinMinEff.value())
            if self.R_e[i] < float(self.ui.spinMinEff.value()):
                self.R_e[i] = float(self.ui.spinMinEff.value()) #R_э_min
            
        for i in self.R_am:
            try:
                self.R_am[i] /= countR_am[i]
            except ZeroDivisionError:
                self.R_am[i] = 0
        self.close()
        
                
    def getR_p(self):
        return self.R_p
        
    def getR_e(self):
        return self.R_e
        
    def getR_am(self):
        return self.R_am
        
    def getActiveValue(self):
        return int(self.ui.comboActiveValue.currentIndex())
        
    def getMinTimeToEvent(self):
        return int(self.ui.spinMinTimeToEvent.value())
        
    def getNumExperiments(self):
        return int(self.ui.spinNumExperiments.value())
                
    def SelectedAll(self):
        for k in range(self.ui.listProtectMeasures.topLevelItemCount()):
            for i in range(self.ui.listProtectMeasures.topLevelItem(k).childCount()):
                for j in range(self.ui.listProtectMeasures.topLevelItem(k).child(i).childCount()):
                    self.ui.listProtectMeasures.topLevelItem(k).child(i).child(j).setCheckState(0, Qt.Checked)
    
    def SelectedNone(self):
        for k in range(self.ui.listProtectMeasures.topLevelItemCount()):
            for i in range(self.ui.listProtectMeasures.topLevelItem(k).childCount()):
                for j in range(self.ui.listProtectMeasures.topLevelItem(k).child(i).childCount()):
                    self.ui.listProtectMeasures.topLevelItem(k).child(i).child(j).setCheckState(0, Qt.Unchecked)
                    
    def SelectedRand(self):
        for k in range(self.ui.listProtectMeasures.topLevelItemCount()):
            for i in range(self.ui.listProtectMeasures.topLevelItem(k).childCount()):
                for j in range(self.ui.listProtectMeasures.topLevelItem(k).child(i).childCount()):
                    t = self.ui.listProtectMeasures.topLevelItem(k).child(i).child(j)
                    t.setCheckState(0, Qt.Unchecked)
                    if random.randint(0, 1) == 1:
                        t.setCheckState(0, Qt.Checked)
        
class ReadFileJson():
    @staticmethod
    def readFileJson(obj, file):
        try:
            f = open(file, 'r', encoding='utf8')
            parse = json.loads(f.read())
            f.close()
        except Exception: #любая другая ошибка
            return []
        else:
            return parse
