# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog
from ui_experiment_time import Ui_ExperimentTime
from PyQt5.QtGui import QIcon
from decimal import Decimal

class ExperimentTime(QDialog):
    def __init__(self):
        super(ExperimentTime, self).__init__()
        self.ui = Ui_ExperimentTime()
        self.ui.setupUi(self)
        self.setModal(True)
        self.setWindowTitle("Пошаговая имитация")
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.ui.experimentBox.valueChanged.connect(self.fillTimes)
        self.ui.incidentBox.currentTextChanged.connect(self.fillTimes)

    def fillTimes(self):
        incidentIndex = self.ui.incidentBox.currentIndex()
        experimentValue = self.ui.experimentBox.value() - 1
        self.ui.t_esk_label.setText(str(self.experiments[incidentIndex]
                                        [experimentValue][0]))
        self.ui.t_sder_label.setText(str(self.experiments[incidentIndex]
                                         [experimentValue][1]))
        self.ui.t_ustr_label.setText(str(self.experiments[incidentIndex]
                                         [experimentValue][2]))
        self.ui.t_vosst_label.setText(str(self.experiments[incidentIndex]
                                          [experimentValue][3]))
        self.ui.sumTimeLabel.setText(str(self.experiments[incidentIndex]
                                          [experimentValue][4]))
        self.ui.previncLabel.setText('<html><head/><body><p><span style=" font-weight:600;">' + self.resObrab[incidentIndex][experimentValue] + "</span></p></body></html>") #полужирный
                        
    def showDialog(self, inc, experiments, resObr):
        self.inc = [i[-1] for i in inc]
        self.resObrab = resObr
        self.experiments = experiments
        self.ui.incidentBox.addItems(self.inc)
        self.ui.incidentBox.setCurrentIndex(0)
        self.ui.experimentBox.setRange(1, len(self.experiments[0]))
        self.ui.experimentBox.setValue(1)
        self.fillTimes()
        self.exec_()
