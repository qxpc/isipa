# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_matrix import Ui_Edit_matrix
from PyQt5.QtCore import Qt
import copy

class Edit_matrix(QDialog):
    def __init__(self):
        super(Edit_matrix, self).__init__()
        self.ui = Ui_Edit_matrix()
        self.ui.setupUi(self)
        self.setWindowTitle('Редактирование: Матрица значимости')
        self.setWindowIcon(QIcon('resources/icon.png'))

        self.ui.matrixTable.itemChanged.connect(self.saveChangedTableItems)
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        self.out = None
        
    def update(self, m):
        self.matrix = copy.deepcopy(m)
        self.fillMatrix()
        
    def fillMatrix(self):
        self.ui.matrixTable.clearContents()            

        for i in range(0, 5):
            for k in range(0, 3):
                self.ui.matrixTable.setItem(i, k, QTableWidgetItem(str(self.matrix[i][k])))

    def saveChangedTableItems(self, item):
        '''сохранить изменения из списка в inc'''
        try:
            tmp = int(item.text())
        except ValueError:
            QMessageBox.critical(self, 'Ошибка', "В поле должно быть натуральное число")
            item.setText(str(self.matrix[item.row()][item.column()]))
        else:
            if tmp < 1:
                QMessageBox.critical(self, 'Ошибка', "В поле должно быть натуральное число")
                item.setText(str(self.matrix[item.row()][item.column()]))
            else:
                self.matrix[item.row()][item.column()] = tmp
                
    def save(self):
        self.out = self.matrix
        self.close()
