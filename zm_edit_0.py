from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QHeaderView, QMessageBox
from PyQt5.QtGui import QIcon
from ui_zm_edit_0 import Ui_ZmEdit
from zm_edit_1 import ZmEdit1
from PyQt5.QtCore import Qt
import copy

class ZmEdit(QDialog):
    def __init__(self):
        super(ZmEdit, self).__init__()
        self.ui = Ui_ZmEdit()
        self.ui.setupUi(self)
        self.setWindowTitle("Редактирование: Стандарт ЗМ")
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.setModal(True)
        self.out = None
        self.zmEdit1 = ZmEdit1()

        self.ui.sectionList.itemChanged.connect(self.changeSec)

        self.ui.addButton.clicked.connect(self.addSec)
        self.ui.removeButton.clicked.connect(self.removeSec)
        self.ui.cancelButton.clicked.connect(self.close)
        self.ui.finishButton.clicked.connect(self.save)
        self.ui.editButton.clicked.connect(self.editSec)

    def buttonSetEnabled(self, status):
        self.ui.removeButton.setEnabled(status)
        self.ui.editButton.setEnabled(status)
        
    def fillSection(self):
        #заполнение разделов
        self.ui.sectionList.clear()
        if len(self.req) == 0:
            self.buttonSetEnabled(False)
        else:
            self.buttonSetEnabled(True)
        for i in self.req:
            self.ui.sectionList.addItem(i['sec'])
            self.ui.sectionList.setCurrentRow(self.ui.sectionList.count() - 1)
            self.ui.sectionList.currentItem().setToolTip(i['sec'])
            self.ui.sectionList.currentItem().setFlags(
                self.ui.sectionList.currentItem().flags() | Qt.ItemIsEditable)
        self.ui.sectionList.setCurrentRow(0)

    def removeSec(self, item):
        row = self.ui.sectionList.currentRow()
        if self.ui.sectionList.count() > 0:
            self.ui.sectionList.takeItem(row)
            del self.req[row]
        if len(self.req) == 0:
            self.buttonSetEnabled(False)
    
    def addSec(self):
        self.zmEdit1.move(self.x() + 25, self.y() + 25)
        self.zmEdit1.showDialog({'sec': '', 'reqList': []})
        if self.zmEdit1.out != None:
            self.req.append(copy.deepcopy(self.zmEdit1.out))
            self.fillSection()

    def save(self):
        if self.ui.sectionList.count() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список разделов пуст')
            return 1
        self.out = self.req
        self.close()

    def editSec(self):
        self.zmEdit1.move(self.x() + 25, self.y() + 25)
        self.zmEdit1.showDialog(self.req[self.ui.sectionList.currentRow()])
        if self.zmEdit1.out != None:
            self.req[self.ui.sectionList.currentRow()] = copy.deepcopy(self.zmEdit1.out)
            self.fillSection()

    def changeSec(self, item):
        if item.text() != "":
            self.req[item.listWidget().row(item)]['sec'] = item.text()
            item.setToolTip(item.text())
        else:
            item.setText(self.req[item.listWidget().row(item)]['sec'])

    def setReq(self, req):
        self.req = copy.deepcopy(req)
