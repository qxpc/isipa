# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././edit_inc_3.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Edit_inc_3(object):
    def setupUi(self, Edit_inc_3):
        Edit_inc_3.setObjectName("Edit_inc_3")
        Edit_inc_3.resize(764, 452)
        self.gridLayout = QtWidgets.QGridLayout(Edit_inc_3)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.saveButton = QtWidgets.QPushButton(Edit_inc_3)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(Edit_inc_3)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.frame4 = QtWidgets.QFrame(Edit_inc_3)
        self.frame4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame4.setObjectName("frame4")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame4)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label = QtWidgets.QLabel(self.frame4)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.factor1List = QtWidgets.QListWidget(self.frame4)
        self.factor1List.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.factor1List.setObjectName("factor1List")
        self.horizontalLayout_6.addWidget(self.factor1List)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.addFactor1Button = QtWidgets.QPushButton(self.frame4)
        self.addFactor1Button.setObjectName("addFactor1Button")
        self.verticalLayout_6.addWidget(self.addFactor1Button)
        self.remFactor1Button = QtWidgets.QPushButton(self.frame4)
        self.remFactor1Button.setObjectName("remFactor1Button")
        self.verticalLayout_6.addWidget(self.remFactor1Button)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem1)
        self.horizontalLayout_6.addLayout(self.verticalLayout_6)
        self.gridLayout_2.addLayout(self.horizontalLayout_6, 3, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.frame4)
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 17))
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 2, 0, 1, 1)
        self.selectedNs1 = QtWidgets.QTableWidget(self.frame4)
        self.selectedNs1.setMinimumSize(QtCore.QSize(0, 30))
        self.selectedNs1.setMaximumSize(QtCore.QSize(16777215, 30))
        self.selectedNs1.setRowCount(1)
        self.selectedNs1.setColumnCount(2)
        self.selectedNs1.setObjectName("selectedNs1")
        self.selectedNs1.horizontalHeader().setVisible(False)
        self.selectedNs1.verticalHeader().setVisible(False)
        self.gridLayout_2.addWidget(self.selectedNs1, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.frame4, 0, 0, 1, 1)

        self.retranslateUi(Edit_inc_3)
        QtCore.QMetaObject.connectSlotsByName(Edit_inc_3)

    def retranslateUi(self, Edit_inc_3):
        _translate = QtCore.QCoreApplication.translate
        Edit_inc_3.setWindowTitle(_translate("Edit_inc_3", "Dialog"))
        self.saveButton.setText(_translate("Edit_inc_3", "Сохранить"))
        self.cancelButton.setText(_translate("Edit_inc_3", "Отмена"))
        self.label.setText(_translate("Edit_inc_3", "Выбранное нежелательное событие 1-го уровня:"))
        self.addFactor1Button.setText(_translate("Edit_inc_3", "Добавить"))
        self.remFactor1Button.setText(_translate("Edit_inc_3", "Удалить"))
        self.label_5.setText(_translate("Edit_inc_3", "Факторы нежелательного события 1-го уровня"))

