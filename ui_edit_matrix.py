# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././edit_matrix.ui'
#
# Created: Sun Jun 26 22:30:59 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Edit_matrix(object):
    def setupUi(self, Edit_matrix):
        Edit_matrix.setObjectName("Edit_matrix")
        Edit_matrix.resize(353, 252)
        self.gridLayout = QtWidgets.QGridLayout(Edit_matrix)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.saveButton = QtWidgets.QPushButton(Edit_matrix)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(Edit_matrix)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.frame2 = QtWidgets.QFrame(Edit_matrix)
        self.frame2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame2.setObjectName("frame2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.matrixTable = QtWidgets.QTableWidget(self.frame2)
        self.matrixTable.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.matrixTable.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.matrixTable.setRowCount(5)
        self.matrixTable.setColumnCount(3)
        self.matrixTable.setObjectName("matrixTable")
        item = QtWidgets.QTableWidgetItem()
        self.matrixTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.matrixTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.matrixTable.setHorizontalHeaderItem(2, item)
        self.matrixTable.horizontalHeader().setVisible(True)
        self.matrixTable.horizontalHeader().setCascadingSectionResizes(False)
        self.matrixTable.horizontalHeader().setSortIndicatorShown(False)
        self.matrixTable.horizontalHeader().setStretchLastSection(True)
        self.matrixTable.verticalHeader().setVisible(True)
        self.matrixTable.verticalHeader().setSortIndicatorShown(False)
        self.matrixTable.verticalHeader().setStretchLastSection(False)
        self.horizontalLayout_3.addWidget(self.matrixTable)
        self.gridLayout_3.addLayout(self.horizontalLayout_3, 1, 0, 1, 2)
        self.gridLayout.addWidget(self.frame2, 0, 0, 1, 1)

        self.retranslateUi(Edit_matrix)
        QtCore.QMetaObject.connectSlotsByName(Edit_matrix)

    def retranslateUi(self, Edit_matrix):
        _translate = QtCore.QCoreApplication.translate
        Edit_matrix.setWindowTitle(_translate("Edit_matrix", "Dialog"))
        self.saveButton.setText(_translate("Edit_matrix", "Сохранить"))
        self.cancelButton.setText(_translate("Edit_matrix", "Отмена"))
        item = self.matrixTable.horizontalHeaderItem(0)
        item.setText(_translate("Edit_matrix", "В"))
        item = self.matrixTable.horizontalHeaderItem(1)
        item.setText(_translate("Edit_matrix", "С"))
        item = self.matrixTable.horizontalHeaderItem(2)
        item.setText(_translate("Edit_matrix", "Н"))

