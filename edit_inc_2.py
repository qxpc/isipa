# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QListWidgetItem, QMessageBox, QFileDialog
from PyQt5.QtGui import QDoubleValidator, QIcon
from ui_edit_inc_2 import Ui_Edit_inc_2
from PyQt5.QtCore import Qt
from edit_inc_3 import Edit_inc_3
import copy

class Edit_inc_2(QDialog):
    def __init__(self):
        super(Edit_inc_2, self).__init__()
        self.ui = Ui_Edit_inc_2()
        self.ui.setupUi(self)
        self.ns2 = []
        self.out = None
        self.setWindowTitle('Редактирование: Нежелательное событие 2-го уровня')
        self.setWindowIcon(QIcon('resources/icon.png'))
        
        self.ui.selectedNs2.setColumnCount(2)
        self.ui.selectedNs2.setSelectionMode(1)
        self.ui.selectedNs2.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.selectedNs2.verticalHeader().setVisible(False)
        self.ui.selectedNs2.horizontalHeader().setVisible(False)
        
        self.ui.ns1Table.setColumnCount(2)
        self.ui.ns1Table.setSelectionMode(1)
        self.ui.ns1Table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.ns1Table.verticalHeader().setVisible(False)
        self.ui.ns1Table.horizontalHeader().setVisible(False)
        
        #события
        self.ui.remFactor2Button.clicked.connect(self.remFac2)
        self.ui.addFactor2Button.clicked.connect(self.addFac2)
        self.ui.addNs1Button.clicked.connect(self.addNs1)
        self.ui.editNs1Button.clicked.connect(self.editNs1)
        self.ui.remNs1Button.clicked.connect(self.remNs1)
        
        self.ui.factor2List.itemChanged.connect(self.saveChangedListItem)
        self.ui.selectedNs2.itemChanged.connect(self.saveChangedTableItems)
        self.ui.ns1Table.itemChanged.connect(self.saveChangedTableItems)
        
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        
    def update(self, ns2):
        self.ns2 = copy.deepcopy(ns2)
        self.ui.selectedNs2.setItem(0, 0, QTableWidgetItem(ns2['name']))    # выбранное
        self.ui.selectedNs2.item(0, 0).setToolTip(ns2['name'])
        self.ui.selectedNs2.setItem(0, 1, QTableWidgetItem(str(ns2['ver']))) #          НС2
        self.fillFactor2List()
        self.fillNs1Table()
        
    def buttonSetEnabled(self, status):
        self.ui.remNs1Button.setEnabled(status)
        self.ui.editNs1Button.setEnabled(status)
        
    def fillFactor2List(self):
        """Заполнение списка факторов"""
        self.ui.factor2List.clear()
        if len(self.ns2['fac']) == 0:
            self.ui.remFactor2Button.setEnabled(False)
        else:
            self.ui.remFactor2Button.setEnabled(True)
            
        for i in self.ns2['fac']:
            self.ui.factor2List.addItem(i)
            self.ui.factor2List.setCurrentRow(self.ui.factor2List.count() - 1)
            self.ui.factor2List.currentItem().setToolTip(i)
            self.ui.factor2List.currentItem().setFlags(
                self.ui.factor2List.currentItem().flags() | Qt.ItemIsEditable)
        
    def fillNs1Table(self):
        """Заполнение"""
        '''Нежелательные события первого уровня'''
        self.ui.ns1Table.clearContents()
        self.ui.ns1Table.setRowCount(0)
        if len(self.ns2['ns1']) == 0:
            self.buttonSetEnabled(False)
        else:
            self.buttonSetEnabled(True)
        for i in self.ns2['ns1']:
            self.ui.ns1Table.insertRow(self.ui.ns1Table.rowCount())
            self.ui.ns1Table.setItem(self.ui.ns1Table.rowCount() - 1, 0, QTableWidgetItem(i['name']))
            self.ui.ns1Table.item(self.ui.ns1Table.rowCount() - 1, 0).setToolTip(i['name'])
            self.ui.ns1Table.setItem(self.ui.ns1Table.rowCount() - 1, 1, QTableWidgetItem(str(i['ver'])))
            
    def editNs1(self):
        '''редактирование выбранного НС2'''
        winEdit3 = Edit_inc_3()
        row = self.ui.ns1Table.currentRow()
        winEdit3.update(self.ns2['ns1'][row])
        winEdit3.move(self.x() + 25, self.y() + 25)
        winEdit3.exec_()

        if winEdit3.out is not None:
            self.ns2['ns1'][row] = copy.deepcopy(winEdit3.out)
            self.fillNs1Table()
            
    def addNs1(self):   
        winEdit3 = Edit_inc_3()
        winEdit3.update({'fac': [], 'name': '', 'ver': ''})
        winEdit3.move(self.x() + 25, self.y() + 25)
        winEdit3.exec_()
    
        if winEdit3.out is not None:
            self.ns2['ns1'].append(copy.deepcopy(winEdit3.out))
            self.fillNs1Table()
            
    def addFac2(self):
        self.ui.remFactor2Button.setEnabled(True)
        self.ns2['fac'].append('<Фактор>')
        self.ui.factor2List.addItem('<Фактор>')
        self.ui.factor2List.setCurrentRow(self.ui.factor2List.count() - 1)
        self.ui.factor2List.currentItem().setFlags(
            self.ui.factor2List.currentItem().flags() | Qt.ItemIsEditable)
        
        self.ui.factor2List.editItem(self.ui.factor2List.currentItem())
        
        
    def remFac2(self):
        '''Удаляет инцидент'''
        if self.ui.factor2List.count() != 0:
            del self.ns2['fac'][self.ui.factor2List.currentRow()]
            self.ui.factor2List.takeItem(self.ui.factor2List.currentRow())
            if len(self.ns2['fac']) == 0:
                self.ui.remFactor2Button.setEnabled(False)
            
    def remNs1(self):
        if self.ui.ns1Table.currentRow() >= 0:
            self.ui.ns1Table.removeRow(self.ui.ns1Table.currentRow())
            del self.ns2['ns1'][self.ui.ns1Table.currentRow()]
            if len(self.ns2['ns1']) == 0:
                self.buttonSetEnabled(False)
        
    def saveChangedListItem(self, item):
        if item.text() != '':
            self.ns2['fac'][item.listWidget().row(item)] = item.text()
            item.setToolTip(item.text())
        else:
            item.setText(self.ns2['fac'][item.listWidget().row(item)])
    
    def errorTableItems(self, item):
        if item.column() == 1:
            if item.tableWidget() is self.ui.ns1Table:
                item.setText(str(self.ns2['ns1'][item.row()]['ver']))
            else:
                item.setText(str(self.ns2['ver']))
        elif item.column() == 0:
            if item.tableWidget() is self.ui.ns1Table:
                item.setText(self.ns2['ns1'][item.row()]['name'])
            else:
                item.setText(self.ns2['name'])
    
    def saveChangedTableItems(self, item):
        if item.text() != '':
            if item.column() == 1:
                try:
                    tmp = float(item.text().replace(',', '.'))
                except ValueError:
                    QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                    self.errorTableItems(item)
                else:
                    if (tmp > 1) or (tmp < 0):
                        QMessageBox.critical(self, 'Ошибка', "В поле должно быть число в диапазоне от 0 до 1")
                        self.errorTableItems(item)
                    else:
                        if item.tableWidget() is self.ui.ns1Table:
                            self.ns2['ns1'][item.row()]['ver'] = tmp
                        else:
                            self.ns2['ver'] = tmp
            elif item.column() == 0:
                if item.tableWidget() is self.ui.ns1Table:
                    self.ns2['ns1'][item.row()]['name'] = item.text()
                    item.setToolTip(item.text())
                else:
                    self.ns2['name'] = item.text()
                    item.setToolTip(item.text())
        else:
            self.errorTableItems(item)

    def save(self):
        '''закрыть диалог с сохранением'''
        tmp = self.ui.selectedNs2.item(0,0).text()
        if tmp != '':
            self.ns2['name'] = tmp
        else:
            QMessageBox.critical(self, 'Ошибка', "Укажите название нежелательного события 2-го уровня")
            self.ui.selectedNs2.setCurrentCell(0, 0)
            self.ui.selectedNs2.setFocus(3)
            return 1
            
        if self.ui.ns1Table.rowCount() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список нежелательных событий пуст')
            return 1
            
        try:
            tmp = float(self.ui.selectedNs2.item(0,1).text().replace(',','.'))
        except ValueError:
            QMessageBox.critical(self, 'Ошибка', "В поле должно быть целое или десятичное число")
            self.ui.selectedNs2.setCurrentCell(0, 1)
            self.ui.selectedNs2.setFocus(3)
            return 1
        else:
            self.ns2['ver'] = tmp
        
        self.out = self.ns2
        self.close()
