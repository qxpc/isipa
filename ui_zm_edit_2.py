# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././zm_edit_2.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ZmEdit2(object):
    def setupUi(self, ZmEdit2):
        ZmEdit2.setObjectName("ZmEdit2")
        ZmEdit2.resize(764, 452)
        self.verticalLayout = QtWidgets.QVBoxLayout(ZmEdit2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.step2Frame = QtWidgets.QFrame(ZmEdit2)
        self.step2Frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.step2Frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.step2Frame.setObjectName("step2Frame")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.step2Frame)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.step2Frame)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.selectedMeasure = QtWidgets.QLineEdit(self.step2Frame)
        self.selectedMeasure.setObjectName("selectedMeasure")
        self.verticalLayout_2.addWidget(self.selectedMeasure)
        self.label_4 = QtWidgets.QLabel(self.step2Frame)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.subTable = QtWidgets.QTableWidget(self.step2Frame)
        self.subTable.setObjectName("subTable")
        self.subTable.setColumnCount(0)
        self.subTable.setRowCount(0)
        self.horizontalLayout.addWidget(self.subTable)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.addButton = QtWidgets.QPushButton(self.step2Frame)
        self.addButton.setObjectName("addButton")
        self.verticalLayout_4.addWidget(self.addButton)
        self.removeButton = QtWidgets.QPushButton(self.step2Frame)
        self.removeButton.setObjectName("removeButton")
        self.verticalLayout_4.addWidget(self.removeButton)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.label_6 = QtWidgets.QLabel(self.step2Frame)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_2.addWidget(self.label_6)
        self.incTable = QtWidgets.QTableWidget(self.step2Frame)
        self.incTable.setMaximumSize(QtCore.QSize(16777215, 125))
        self.incTable.setObjectName("incTable")
        self.incTable.setColumnCount(0)
        self.incTable.setRowCount(0)
        self.verticalLayout_2.addWidget(self.incTable)
        self.verticalLayout.addWidget(self.step2Frame)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.saveButton = QtWidgets.QPushButton(ZmEdit2)
        self.saveButton.setMinimumSize(QtCore.QSize(0, 0))
        self.saveButton.setMaximumSize(QtCore.QSize(175, 36))
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout_2.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(ZmEdit2)
        self.cancelButton.setMinimumSize(QtCore.QSize(0, 0))
        self.cancelButton.setMaximumSize(QtCore.QSize(175, 36))
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout_2.addWidget(self.cancelButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ZmEdit2)
        QtCore.QMetaObject.connectSlotsByName(ZmEdit2)

    def retranslateUi(self, ZmEdit2):
        _translate = QtCore.QCoreApplication.translate
        ZmEdit2.setWindowTitle(_translate("ZmEdit2", "Dialog"))
        self.label.setText(_translate("ZmEdit2", "Выбранная защитная мера 1-го уровня:"))
        self.label_4.setText(_translate("ZmEdit2", "Защитные меры 2-го уровня:"))
        self.addButton.setText(_translate("ZmEdit2", "Добавить"))
        self.removeButton.setText(_translate("ZmEdit2", "Удалить"))
        self.label_6.setText(_translate("ZmEdit2", "Коэффициенты результативности ЗМ:"))
        self.saveButton.setText(_translate("ZmEdit2", "Сохранить"))
        self.cancelButton.setText(_translate("ZmEdit2", "Отмена"))

