from PyQt5.QtWidgets import QDialog, QTableWidgetItem, QHeaderView, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
from ui_zm_edit_1 import Ui_ZmEdit1
from zm_edit_2 import ZmEdit2
import copy

class ZmEdit1(QDialog):
    def __init__(self):
        super(ZmEdit1, self).__init__()
        self.ui = Ui_ZmEdit1()
        self.ui.setupUi(self)
        self.setWindowTitle('Редактирование: Раздел')
        self.setWindowIcon(QIcon('resources/icon.png'))
        self.setModal(True)
        self.out = None
        self.zmEdit2 = ZmEdit2()
        

        self.ui.reqList.itemChanged.connect(self.setReqItemName)
        self.ui.addButton.clicked.connect(self.addReq)
        self.ui.removeButton.clicked.connect(self.removeReq)
        self.ui.saveButton.clicked.connect(self.save)
        self.ui.cancelButton.clicked.connect(self.close)
        self.ui.editButton.clicked.connect(self.editSub)

    def showDialog(self, sec):
        self.sec = copy.deepcopy(sec)
        self.ui.selectedSec.setText(self.sec['sec'])
        self.fillReqTable()
        self.exec_()

    def buttonSetEnabled(self, status):
        self.ui.removeButton.setEnabled(status)
        self.ui.editButton.setEnabled(status)
        
    def fillReqTable(self):
        self.ui.reqList.clear()
        if len(self.sec['reqList']) == 0:
            self.buttonSetEnabled(False)
        else:
            self.buttonSetEnabled(True)
        for i in self.sec['reqList']:
            self.ui.reqList.addItem(i['req'])
            self.ui.reqList.setCurrentRow(self.ui.reqList.count() - 1)
            self.ui.reqList.currentItem().setToolTip(i['req'])
            self.ui.reqList.currentItem().setFlags(
                self.ui.reqList.currentItem().flags() | Qt.ItemIsEditable)
        self.ui.reqList.setCurrentRow(0)
        
    def addReq(self):
        curRow = self.ui.reqList.currentRow()
        self.zmEdit2.move(self.x() + 25, self.y() + 25)
        self.zmEdit2.showDialog({'req': '', 'inc': [], 'sub': []})
        if self.zmEdit2.out != None:
            self.sec['reqList'].append(copy.deepcopy(self.zmEdit2.out))
            self.fillReqTable()
        
    def setReqItemName(self, item):
        if item.text() != "":
            self.sec['reqList'][item.listWidget().row(item)]['req'] = item.text()
            item.setToolTip(item.text())
        else:
            item.setText(self.sec['reqList'][item.listWidget().row(item)]['req'])

    def removeReq(self):
        row = self.ui.reqList.currentRow()
        if self.ui.reqList.count() > 0:
            self.ui.reqList.takeItem(row)
            del self.sec['reqList'][row]
        if len(self.sec['reqList']) == 0:
            self.buttonSetEnabled(False)
        
    def save(self):
        if self.ui.selectedSec.text() == '':
            QMessageBox.critical(self, 'Ошибка', 'Укажите название раздела')
            return 1
        if self.ui.reqList.count() == 0:
            QMessageBox.critical(self, 'Ошибка', 'Список защитных мер пуст')
            return 1
        self.sec['sec'] = self.ui.selectedSec.text()
        self.out = self.sec
        self.close()

    def editSub(self):
        curRow = self.ui.reqList.currentRow()
        self.zmEdit2.move(self.x() + 25, self.y() + 25)
        self.zmEdit2.showDialog(self.sec['reqList'][curRow])

        if self.zmEdit2.out != None:
            self.sec['reqList'][curRow] = copy.deepcopy(self.zmEdit2.out)
            self.fillReqTable()
