# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././incidentprobabilities.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_IncidentProbabilities(object):
    def setupUi(self, IncidentProbabilities):
        IncidentProbabilities.setObjectName("IncidentProbabilities")
        IncidentProbabilities.resize(651, 378)
        self.verticalLayout = QtWidgets.QVBoxLayout(IncidentProbabilities)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.procResult = QtWidgets.QTableWidget(IncidentProbabilities)
        self.procResult.setObjectName("procResult")
        self.procResult.setColumnCount(0)
        self.procResult.setRowCount(0)
        self.gridLayout.addWidget(self.procResult, 1, 1, 1, 1)
        self.incProb = QtWidgets.QTableWidget(IncidentProbabilities)
        self.incProb.setObjectName("incProb")
        self.incProb.setColumnCount(0)
        self.incProb.setRowCount(0)
        self.gridLayout.addWidget(self.incProb, 1, 0, 1, 1)
        self.label = QtWidgets.QLabel(IncidentProbabilities)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(IncidentProbabilities)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.label_3 = QtWidgets.QLabel(IncidentProbabilities)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.spinBoxSPrime = QtWidgets.QDoubleSpinBox(IncidentProbabilities)
        self.spinBoxSPrime.setDecimals(3)
        self.spinBoxSPrime.setMinimum(-1.0)
        self.spinBoxSPrime.setMaximum(1.0)
        self.spinBoxSPrime.setSingleStep(0.1)
        self.spinBoxSPrime.setObjectName("spinBoxSPrime")
        self.horizontalLayout_2.addWidget(self.spinBoxSPrime)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem2 = QtWidgets.QSpacerItem(384, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.okButton = QtWidgets.QPushButton(IncidentProbabilities)
        self.okButton.setObjectName("okButton")
        self.horizontalLayout.addWidget(self.okButton)
        self.cancelButton = QtWidgets.QPushButton(IncidentProbabilities)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(IncidentProbabilities)
        QtCore.QMetaObject.connectSlotsByName(IncidentProbabilities)

    def retranslateUi(self, IncidentProbabilities):
        _translate = QtCore.QCoreApplication.translate
        IncidentProbabilities.setWindowTitle(_translate("IncidentProbabilities", "Dialog"))
        self.label.setText(_translate("IncidentProbabilities", "Виды инцидентов"))
        self.label_2.setText(_translate("IncidentProbabilities", "Результаты обработки"))
        self.label_3.setText(_translate("IncidentProbabilities", "S\' = "))
        self.okButton.setText(_translate("IncidentProbabilities", "ОК"))
        self.cancelButton.setText(_translate("IncidentProbabilities", "Отмена"))

