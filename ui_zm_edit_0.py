# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././zm_edit_0.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ZmEdit(object):
    def setupUi(self, ZmEdit):
        ZmEdit.setObjectName("ZmEdit")
        ZmEdit.resize(764, 452)
        self.gridLayout_4 = QtWidgets.QGridLayout(ZmEdit)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.finishButton = QtWidgets.QPushButton(ZmEdit)
        self.finishButton.setMinimumSize(QtCore.QSize(0, 0))
        self.finishButton.setMaximumSize(QtCore.QSize(175, 36))
        self.finishButton.setObjectName("finishButton")
        self.horizontalLayout_2.addWidget(self.finishButton)
        self.cancelButton = QtWidgets.QPushButton(ZmEdit)
        self.cancelButton.setMinimumSize(QtCore.QSize(0, 0))
        self.cancelButton.setMaximumSize(QtCore.QSize(175, 36))
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout_2.addWidget(self.cancelButton)
        self.gridLayout_4.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.frame = QtWidgets.QFrame(ZmEdit)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 1, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.addButton = QtWidgets.QPushButton(self.frame)
        self.addButton.setObjectName("addButton")
        self.verticalLayout_3.addWidget(self.addButton)
        self.removeButton = QtWidgets.QPushButton(self.frame)
        self.removeButton.setObjectName("removeButton")
        self.verticalLayout_3.addWidget(self.removeButton)
        self.editButton = QtWidgets.QPushButton(self.frame)
        self.editButton.setObjectName("editButton")
        self.verticalLayout_3.addWidget(self.editButton)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.gridLayout.addLayout(self.verticalLayout_3, 3, 2, 1, 1)
        self.sectionList = QtWidgets.QListWidget(self.frame)
        self.sectionList.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.sectionList.setObjectName("sectionList")
        self.gridLayout.addWidget(self.sectionList, 3, 1, 1, 1)
        self.gridLayout_4.addWidget(self.frame, 0, 0, 1, 1)

        self.retranslateUi(ZmEdit)
        QtCore.QMetaObject.connectSlotsByName(ZmEdit)

    def retranslateUi(self, ZmEdit):
        _translate = QtCore.QCoreApplication.translate
        ZmEdit.setWindowTitle(_translate("ZmEdit", "Dialog"))
        self.finishButton.setText(_translate("ZmEdit", "Сохранить"))
        self.cancelButton.setText(_translate("ZmEdit", "Отмена"))
        self.label_2.setText(_translate("ZmEdit", "Разделы:"))
        self.addButton.setText(_translate("ZmEdit", "Добавить..."))
        self.removeButton.setText(_translate("ZmEdit", "Удалить"))
        self.editButton.setText(_translate("ZmEdit", "Изменить..."))

