# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '././zm_edit_1.ui'
#
# Created: Sun Jun 26 22:31:00 2016
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ZmEdit1(object):
    def setupUi(self, ZmEdit1):
        ZmEdit1.setObjectName("ZmEdit1")
        ZmEdit1.resize(764, 452)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(ZmEdit1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.step1Frame = QtWidgets.QFrame(ZmEdit1)
        self.step1Frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.step1Frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.step1Frame.setObjectName("step1Frame")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.step1Frame)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.selectedSec = QtWidgets.QLineEdit(self.step1Frame)
        self.selectedSec.setObjectName("selectedSec")
        self.gridLayout_3.addWidget(self.selectedSec, 5, 0, 1, 2)
        self.label_5 = QtWidgets.QLabel(self.step1Frame)
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.step1Frame)
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 6, 0, 1, 2)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.addButton = QtWidgets.QPushButton(self.step1Frame)
        self.addButton.setObjectName("addButton")
        self.verticalLayout.addWidget(self.addButton)
        self.removeButton = QtWidgets.QPushButton(self.step1Frame)
        self.removeButton.setObjectName("removeButton")
        self.verticalLayout.addWidget(self.removeButton)
        self.editButton = QtWidgets.QPushButton(self.step1Frame)
        self.editButton.setMinimumSize(QtCore.QSize(0, 0))
        self.editButton.setObjectName("editButton")
        self.verticalLayout.addWidget(self.editButton)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.gridLayout_3.addLayout(self.verticalLayout, 7, 1, 1, 1)
        self.reqList = QtWidgets.QListWidget(self.step1Frame)
        self.reqList.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.reqList.setObjectName("reqList")
        self.gridLayout_3.addWidget(self.reqList, 7, 0, 1, 1)
        self.verticalLayout_2.addWidget(self.step1Frame)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.saveButton = QtWidgets.QPushButton(ZmEdit1)
        self.saveButton.setMinimumSize(QtCore.QSize(0, 0))
        self.saveButton.setMaximumSize(QtCore.QSize(175, 36))
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout_2.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(ZmEdit1)
        self.cancelButton.setMinimumSize(QtCore.QSize(0, 0))
        self.cancelButton.setMaximumSize(QtCore.QSize(175, 36))
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout_2.addWidget(self.cancelButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ZmEdit1)
        QtCore.QMetaObject.connectSlotsByName(ZmEdit1)

    def retranslateUi(self, ZmEdit1):
        _translate = QtCore.QCoreApplication.translate
        ZmEdit1.setWindowTitle(_translate("ZmEdit1", "Dialog"))
        self.label_5.setText(_translate("ZmEdit1", "Выбранный раздел:"))
        self.label_3.setText(_translate("ZmEdit1", "Защитные меры 1-го уровня:"))
        self.addButton.setText(_translate("ZmEdit1", "Добавить..."))
        self.removeButton.setText(_translate("ZmEdit1", "Удалить"))
        self.editButton.setText(_translate("ZmEdit1", "Изменить..."))
        self.saveButton.setText(_translate("ZmEdit1", "Сохранить"))
        self.cancelButton.setText(_translate("ZmEdit1", "Отмена"))

